#!/usr/bin/python3


import numpy as np
from math import acos
from math import cos
from math import sqrt
from math import ceil
from math import floor
from copy import deepcopy

#Cosas que mejorar
#pasivation hBN
#que te diga el numero de atomos y el tamano de la celula
#xsf para vesta


import numpy as np
from math import ceil
import itertools as it


##################    Input structure 2D  ######################
##Graphene
Gr_cell = np.array( [ [ 2.45999700 , 0.00000000 , 0.00000000 ] ,
                      [-1.22999850 , 2.13041890 , 0.00000000 ] ,
                      [ 0.00000000 , 0.00000000 , 15.0000000 ] ] )
##hBN
hBN_cell = np.array( [ [ 2.5000000 , 0.00000000 , 0.00000000 ] ,
                       [-1.2500000 , 2.16506250 , 0.00000000 ] ,
                       [ 0.0000000 , 0.00000000 , 15.0000000 ] ] )

##Crystal position structure
atms_cryst_pos = np.array( [ [ 0.33333333 , 0.66666667 , 0.000000 ] ,
                             [ 0.66666667 , 0.33333333 , 0.000000 ] ] )

################################################################


def rotation(angle , x , y ):
    theta = np.radians(angle)
    c , s = np.cos(theta) , np.sin(theta)
    R = np.array( ( ( c , s ) , ( -s , c ) ) )
    l_in = np.array( ( x , y ) )
    l_out = np.dot(l_in,R)

    return (l_out[0] , l_out[1] )

def rotate_structure( angle , structure ) :
    for i in range ( len (structure ) ):
        rot_pos = rotation( angle , structure[ i ][ 0 ] , structure[ i ][ 1 ] )
        structure [ i ] = [ rot_pos[ 0 ] , rot_pos[ 1 ] , structure[ i ][ 2 ], structure[ i ][ 3 ] ]
    return structure

def create_triangle_armchair ( size , material ) :
    global Gr_cell
    global hBN_cell
    global atms_cryst_pos

    # creating list of dimmer corrdiantes in crystal coord
    l_perm = [ i for i in range ( size+1 )  ]
    dimer_coord = list ( it.product( l_perm , repeat= 2 )  )
    aux = []
    for i in dimer_coord : 
        if sum(i) > size+1 :  aux.append(i)
    for i in aux :
        dimer_coord.remove(i)

    # removing dimer to create armchair triangle
    dimer_coord.append( ( 0 , size+1 ) )
    dimer_coord.append( ( size+1 , 0 ) )

    if material == 'Gr'  : cell =  Gr_cell ; type_at = [ 'C' , 'C' ]
    if material == 'hBN' : cell = hBN_cell ; type_at = [ 'B' , 'N' ]
    atoms_coord = []
    ## function to obtain coordinates
    for i in range ( len(dimer_coord) ) :
        atoms_coord.append([ ( (dimer_coord[i][0]+atms_cryst_pos[0,0])*cell[0,0] + (dimer_coord[i][1]+atms_cryst_pos[0,1])*cell[1,0] ) , ( (dimer_coord[i][0]+atms_cryst_pos[0,0])*cell[0,1] + (dimer_coord[i][1]+atms_cryst_pos[0,1])*cell[1,1] ) , 0.00000000 , type_at[0] ])
        atoms_coord.append([ ( (dimer_coord[i][0]+atms_cryst_pos[1,0])*cell[0,0] + (dimer_coord[i][1]+atms_cryst_pos[1,1])*cell[1,0] ) , ( (dimer_coord[i][0]+atms_cryst_pos[1,0])*cell[0,1] + (dimer_coord[i][1]+atms_cryst_pos[1,1])*cell[1,1] ) , 0.00000000 , type_at[1] ])


    for i in range ( len(dimer_coord)*2 ) :
        coord_rot = rotation( 120 , atoms_coord[i][0] , atoms_coord[i][1] )
        atoms_coord.append( [ coord_rot[0] , coord_rot[1] , 0.00000000 , atoms_coord[i][3] ] )

        coord_rot = rotation( 240 , atoms_coord[i][0] , atoms_coord[i][1] )
        atoms_coord.append( [ coord_rot[0] , coord_rot[1] , 0.00000000 , atoms_coord[i][3] ] )

    return (atoms_coord)

    return


def line ( a1 , a2 , x ):

    l = a1[1]+ ( a1[1]- a2[1] )/(a1[0] - a2[0] ) * ( x-a1[0] )
    return(l)

def create_hexagon_armchair ( size , material ) :
    global Gr_cell
    global hBN_cell
    global atms_cryst_pos


    # creating list of dimmer corrdiantes in crystal coord
    ###### The size will be define by th enumber of armchair holes
    l_perm = [ i for i in range ( size *2 + 1  )  ] ## defines the highest peak (3,6) 
    dimer_coord = list ( it.product( l_perm , repeat= 2 ) )



    if material == 'Gr'  : cell =  Gr_cell ; type_at = [ 'C' , 'C' ]
    if material == 'hBN' : cell = hBN_cell ; type_at = [ 'B' , 'N' ]



    atoms_coord = []
    ## function to obtain coordinates
    ## Until here is an ordinate hexagonla cell constructuion
    for i in range ( len(dimer_coord) ) :
        atoms_coord.append([ ( (dimer_coord[i][0]+atms_cryst_pos[0,0])*cell[0,0] + (dimer_coord[i][1]+atms_cryst_pos[0,1])*cell[1,0] ) , ( (dimer_coord[i][0]+atms_cryst_pos[0,0])*cell[0,1] + (dimer_coord[i][1]+atms_cryst_pos[0,1])*cell[1,1] ) , 0.00000000 , type_at[0] ]) 
        atoms_coord.append([ ( (dimer_coord[i][0]+atms_cryst_pos[1,0])*cell[0,0] + (dimer_coord[i][1]+atms_cryst_pos[1,1])*cell[1,0] ) , ( (dimer_coord[i][0]+atms_cryst_pos[1,0])*cell[0,1] + (dimer_coord[i][1]+atms_cryst_pos[1,1])*cell[1,1] ) , 0.00000000 , type_at[1] ])

 
   ## Definition des limites de notre armchair hexagon dot with size 3*n-1 (size 3n+1 are not allowed)
    l_a2 = ( ((size + atms_cryst_pos[0,0])*cell[0][0] + (size*2 + atms_cryst_pos[0,1])*cell[1][0] , (size + atms_cryst_pos[0,0])*cell[0][1] + ( size*2 +atms_cryst_pos[0,1])*cell[1][1]))
    r_a2 = ( (size*2 + atms_cryst_pos[1,0])*cell[0][0] + (size + atms_cryst_pos[1,1])*cell[1][0] , (size*2 + atms_cryst_pos[1,0])*cell[0][1] + (size + atms_cryst_pos[1,1])*cell[1][1])

    if (size%2 == 0) :
        r_a1 = ( (size*1.5 +atms_cryst_pos[1,0])*cell[0][0] +atms_cryst_pos[1,1]*cell[1][0] , (size*1.5 +atms_cryst_pos[1,0])*cell[0][1] +atms_cryst_pos[1,1]*cell[1][1] )
        l_a1 = ( atms_cryst_pos[0,0]*cell[0][0]+(size*1.5 +atms_cryst_pos[0,1])*cell[1][0] , atms_cryst_pos[0,0]*cell[0][1]+(size*1.5 +atms_cryst_pos[0,1])*cell[1][1] )
    if (size%2 != 0) :
        r_a1 =  ( ( ceil(size*1.5) +atms_cryst_pos[0,0])*cell[0][0] +atms_cryst_pos[0,1]*cell[1][0] , (ceil(size*1.5) +atms_cryst_pos[0,0])*cell[0][1] +atms_cryst_pos[0,1]*cell[1][1])
        l_a1 =  ( atms_cryst_pos[1,0]*cell[0][0]+( ceil(size*1.5) +atms_cryst_pos[1,1])*cell[1][0] , atms_cryst_pos[1,1]*cell[0][1]+(ceil(size*1.5) +atms_cryst_pos[1,1])*cell[1][1] )
       

    aux =[]
    for i in atoms_coord :
        l_limit = line( l_a1 , l_a2 , i[0]) + 0.1
        t_limit = line( l_a2 , r_a2 , i[0]) + 0.1
        r_limit = r_a1[0] + 0.1

        if (( i[1] > l_limit ) or (i[0] > r_limit) or ((i[0]>l_a2[0]) and (i[1]>t_limit))) : aux.append(i)
    for i in aux : atoms_coord.remove(i)

    for i in range ( len(dimer_coord)*2 ) :
        coord_rot = rotation( 120 , atoms_coord[i][0] , atoms_coord[i][1] )
        atoms_coord.append( [ coord_rot[0] , coord_rot[1] , 0.00000000 , atoms_coord[i][3] ] )

        coord_rot = rotation( 240 , atoms_coord[i][0] , atoms_coord[i][1] )
        atoms_coord.append( [ coord_rot[0] , coord_rot[1] , 0.00000000 , atoms_coord[i][3] ] )

    return (atoms_coord)


def create_hexagon ( size , material ) :
    global Gr_cell
    global hBN_cell
    global atms_cryst_pos

    l_perm = [ i for i in range ( size ) ]
    dimer_coord = list ( it.product( l_perm , repeat= 2 ) )

    if material == 'Gr'  : cell =  Gr_cell ; type_at = [ 'C' , 'C' ]
    if material == 'hBN' : cell = hBN_cell ; type_at = [ 'B' , 'N' ]

    atoms_coord = []
    ## function to obtain coordinates
    for i in range ( len(dimer_coord) ) :
        atoms_coord.append([ ( (dimer_coord[i][0]+atms_cryst_pos[0,0])*cell[0,0] + (dimer_coord[i][1]+atms_cryst_pos[0,1])*cell[1,0] ) , ( (dimer_coord[i][0]+atms_cryst_pos[0,0])*cell[0,1] + (dimer_coord[i][1]+atms_cryst_pos[0,1])*cell[1,1] ) , 0.00000000 , type_at[0] ])
        atoms_coord.append([ ( (dimer_coord[i][0]+atms_cryst_pos[1,0])*cell[0,0] + (dimer_coord[i][1]+atms_cryst_pos[1,1])*cell[1,0] ) , ( (dimer_coord[i][0]+atms_cryst_pos[1,0])*cell[0,1] + (dimer_coord[i][1]+atms_cryst_pos[1,1])*cell[1,1] ) , 0.00000000 , type_at[1] ])


    return (atoms_coord)


def distance ( x1 , y1 , x2 , y2 ):
    d = sqrt( ( x1 - x2 ) **2 + ( y1 - y2 ) **2 )
    return d



def deplace_structure(structure, deplacement , material):
    global Gr_cell
    global hBN_cell


    sign=0
    diff = ( hBN_cell - Gr_cell ) / 4

    if material == 'Gr'  : cell =  Gr_cell#; sign = -1
    if material == 'hBN' : cell = hBN_cell#; sign = +1



    #deplacement = np.resize( deplacement, ( len( structure) , 2 ) )
    for i in range ( len( structure) ) :
       #structure[i][0] += deplacement*( cell[0][0] + sign*diff[0][0] + cell[1][0] + sign*diff[1][0] )
       #structure[i][1] += deplacement*( cell[0][1] + sign*diff[0][1] + cell[1][1] + sign*diff[1][0] )
        structure[i][0] += deplacement[0]*cell[0][0] + sign*diff[0][0] + deplacement[1]*cell[1][0] + sign*diff[1][0] 
        structure[i][1] += deplacement[0]*cell[0][1] + sign*diff[0][1] + deplacement[1]*cell[1][1] + sign*diff[1][0] 
    return structure



def eliminate_double_atoms( structure_basis , structure_island ) :
    index = -1
    for j in range ( len(structure_island ) ) :
        for i in range (len(structure_basis) ) :
            dist = distance ( structure_basis[i][0] , structure_basis[i][1] , structure_island[j][0] , structure_island[j][1])
            if dist < 1.0 :         index = i
        if index != -1 :    del structure_basis[index]
        index = -1
                    
    return structure_basis





def write_xsf_file(structure , material , basis_size , output_name):
    global Gr_cell
    global hBN_cell
    
    if material == 'Gr'  : cell =  Gr_cell
    if material == 'hBN' : cell = hBN_cell

    w=open(output_name, 'w')

    w.write(' CRYSTAL \n')
    w.write(' PRIMVEC\n')
    w.write('%10.6f %10.6f %10.6f \n' % tuple(cell[0]*basis_size))
    w.write('%10.6f %10.6f %10.6f \n' % tuple(cell[1]*basis_size))
    w.write('%10.6f %10.6f %10.6f \n' % tuple(cell[2]))
    w.write(' PRIMCOORD \n')
    w.write('%10i %10i \n' %( len(structure) ,1 ) )
    for i in range ( len(structure) ):
        w.write(' %3s  %10.6f  %10.6f  %10.6f     #  %3i\n' % ( structure[i][3] , structure[i][0] , structure[i][1] , structure[i][2], i+1))
        
    w.close()



###########################################

island_shape , island_material  = ( input( ' ###  Shape ( hexagon, triangle ) and material ( Gr , hBN ) of islands    :') ).split()
island_size  , basis_size       = ( input( ' ### Size of islands and primitive cell                                   :') ).split()
output_name                     =   input( ' ### xsf file name                                                        :')                               



#obtain basis material
if island_material == 'hBN' : basis_material = 'Gr' 
if island_material == 'Gr'  : basis_material = 'hBN'

#Built structures

if island_shape == 'hexagon'  : structure_island = create_hexagon_armchair( int(island_size) , island_material )
if island_shape == 'triangle' : structure_island = create_triangle_armchair( int(island_size) , island_material )
structure_basis  = create_hexagon( int(basis_size) , basis_material )
if (len(structure_basis ) < len(structure_island) ) : print( "Island structure bigger than basis structure ") ; exit()
write_xsf_file( structure_basis , basis_material , int(basis_size) , 'hBN.xsf')


#Superpose structures
structure_island = deplace_structure( structure_island , [int(int(basis_size)/2),int(int(basis_size)/2)] , basis_material )
structure_basis  = eliminate_double_atoms( structure_basis , structure_island )
structure_total  = structure_basis + structure_island 

# write structure file xsf
write_xsf_file( structure_total , basis_material , int(basis_size) , output_name )




##
#structure = create_hexagon( 12 , 'hBN' )
#structure_hBN_2 = create_hexagon( 6 , 'hBN' )
#structure_Gr_1 = create_hexagon(  6 ,'Gr' )
#structure_Gr_2 = create_hexagon(  6 ,'Gr' )

#structure_hBN_2 = deplace_structure( structure_hBN_2 ,[0,6] ,'hBN')
#structure_Gr_1 = deplace_structure( structure_Gr_1 ,[6,6] ,'hBN')
#structure_Gr_2 = deplace_structure( structure_Gr_2 ,[6,0] , 'hBN')


#structure = structure_hBN_1+structure_hBN_2+structure_Gr_1+structure_Gr_2
#write_xsf_file( structure, 'hBN', 12, 'interface_hBN_mixed.xsf')
