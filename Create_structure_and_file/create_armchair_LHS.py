#!/usr/bin/python3


import numpy as np
from math import acos
from math import cos
from math import sqrt
from math import ceil
from math import floor

#Cosas que mejorar
#pasivation hBN
#que te diga el numero de atomos y el tamano de la celula
#xsf para vesta


import numpy as np
from math import ceil




############# INPUT

### Input structure file (txt file from vesta)
##Graphene
Gr_rect = np.array([[0.0000000, 2.8405592, 0.000000],
                    [0.0000000, 0.0000000, 0.000000],
                    [1.2299978, 0.7101394, 0.000000],
                    [1.2299978, 2.1304199, 0.000000]])

acell_Gr_rect = np.array([2.459997,4.2608397,0.0])

##hBN
hBN_rect = np.array([[0.0000000, 2.8867472, 0.00000000],
                     [0.0000000, 0.0000000, 0.00000000],
                     [1.2499982, 0.7216872, 0.00000000],
                     [1.2499982, 2.1650600, 0.00000000]])

acell_hBN_rect = np.array([2.499996,4.330120,0.0])



rows_in = input('##### Gr and BN raws   :')
rows = [[],[]]
rows = list(map(int, rows_in.split()))


def create_lattice(rows):
    global acell_Gr_rect
    global acell_hBN_rect

    latt = [ [] , [] , [] ]
    if (rows[0]%2 == 0):
        latt[0] = [(acell_Gr_rect[0]*rows[0]+acell_hBN_rect[0]*rows[1]) /2+0.017*2 , 0.00000000 , 0.00000000]
    if (rows[0]%2 != 0):
        latt[0] = [(acell_Gr_rect[0]*rows[0]+acell_hBN_rect[0]*(rows[1])) /2 +0.017*3, 0.00000000 , 0.00000000]
    latt[1] = [ 0.00000000, (acell_Gr_rect[1]+acell_hBN_rect[1]) /2 , 0.00000000]
    latt[2] = [ 0.00000000, 0.00000000, 15.00000000]

    return (latt)


def create_structure(rows):
    global Gr_rect
    global hBN_rect
    global acell_Gr_rect
    global acell_hBN_rect

    structure = np.zeros([sum(rows)*2,3])
    type_atoms = []
     
    for i in range(ceil(rows[0]/2)-1):
        structure[i*4]   = Gr_rect[0] + [acell_Gr_rect[0]*i, 0.00000000, 0.00000000 ]
        type_atoms.append('C')
        structure[i*4+1] = Gr_rect[1] + [acell_Gr_rect[0]*i, 0.00000000, 0.00000000 ]
        type_atoms.append('C')
        structure[i*4+2] = Gr_rect[2] + [acell_Gr_rect[0]*i, 0.00000000, 0.00000000 ]
        type_atoms.append('C')
        structure[i*4+3] = Gr_rect[3] + [acell_Gr_rect[0]*i, 0.00000000, 0.00000000 ]
        type_atoms.append('C')
    
    i_aux= (i+1)
    if (rows[0]%2 == 0):
        structure[i_aux*4]   = Gr_rect[0] + [acell_Gr_rect[0]*i_aux, 0.00000000, 0.00000000 ]
        type_atoms.append('C')
        structure[i_aux*4+1] = Gr_rect[1] + [acell_Gr_rect[0]*i_aux, 0.00000000, 0.00000000 ]
        type_atoms.append('C')
        structure[i_aux*4+2] = Gr_rect[2] + [acell_Gr_rect[0]*i_aux, 0.00000000, 0.00000000 ]
        type_atoms.append('C')
        structure[i_aux*4+3] = Gr_rect[3] + [acell_Gr_rect[0]*i_aux, 0.00000000, 0.00000000 ]
        type_atoms.append('C')
        Gr_size=acell_Gr_rect[0]*(i_aux+1/2)+acell_hBN_rect[0]*(1/2)

    if (rows[0]%2 != 0):
         structure[i_aux*4]   = Gr_rect[0] + [acell_Gr_rect[0]*i_aux, 0.00000000, 0.00000000 ]
         type_atoms.append('C')
         structure[i_aux*4+1] = Gr_rect[1] + [acell_Gr_rect[0]*i_aux, 0.00000000, 0.00000000 ]
         type_atoms.append('C')
         structure[i_aux*4+2] = hBN_rect[2] + [acell_Gr_rect[0]*i_aux+0.017, 0.00000000, 0.00000000 ]
         type_atoms.append('B')
         structure[i_aux*4+3] = hBN_rect[3] + [acell_Gr_rect[0]*i_aux+0.017, 0.00000000, 0.00000000 ]
         type_atoms.append('N')
         Gr_size=acell_Gr_rect[0]*(i_aux)+acell_hBN_rect[0]+0.017
  
    natoms = (i_aux+1)*4
    for i in range(floor(rows[1]/2)):
         structure[natoms+i*4]   = hBN_rect[0] + [Gr_size+acell_hBN_rect[0]*i, 0.00000000, 0.00000000 ]
         type_atoms.append('B')
         structure[natoms+i*4+1] = hBN_rect[1] + [Gr_size+acell_hBN_rect[0]*i, 0.00000000, 0.00000000 ]
         type_atoms.append('N')
         structure[natoms+i*4+2] = hBN_rect[2] + [Gr_size+acell_hBN_rect[0]*i, 0.00000000, 0.00000000 ]
         type_atoms.append('B')
         structure[natoms+i*4+3] = hBN_rect[3] + [Gr_size+acell_hBN_rect[0]*i, 0.00000000, 0.00000000 ]
         type_atoms.append('N')
  
  
  
    return (structure,type_atoms)
  


def write_xsf_file(structure,acell,atoms):
    global rows

    name_xsf = 'structure_G'+str(rows[0])+'_BN'+str(rows[1])+'_0.xsf'
    w=open(name_xsf, 'w')

    w.write(' CRYSTAL \n')
    w.write(' PRIMVEC\n')
    w.write('%10.6f %10.6f %10.6f \n' % tuple(latt[0]))
    w.write('%10.6f %10.6f %10.6f \n' % tuple(latt[1]))
    w.write('%10.6f %10.6f %10.6f \n' % tuple(latt[2]))
    w.write(' PRIMCOORD \n')
    w.write('%10i %10i \n' %(sum(rows)*2,1))
    for i in range (sum(rows)*2):
        w.write(' %3s  %10.6f  %10.6f  %10.6f     #  %3i\n' % ( atoms[i], structure[i,0], structure[i,1], structure[i,2], i+1))
        
    w.close()



latt = create_lattice(rows)
structure = create_structure(rows)
atoms = structure[1]
coordinates = structure[0]

write_xsf_file(coordinates,latt,atoms)

   
