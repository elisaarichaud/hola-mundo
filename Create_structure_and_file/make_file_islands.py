#!/usr/bin/python3

import sys

""" This program write the input file for twisted_TB 
    reading the structure from an xsf file """




structure_in = input(' ### xsf file : ')



onsite_n = -3.48
onsite_b = 4.20
onsite_c = 0.0
tcc_first = -4.00
tbn_first = -3.6
tcn_first = -6.9
tcb_first = -0.45






def read_structure_xsf(structure):

    fin = open(structure,'r') ; lines = fin.readlines() ; fin.close()
    for il in range(len(lines)):
        line = lines[il].split()
        if line[0] == 'PRIMCOORD' : 
            natoms = int(lines[il+1].split()[0])
            il_coords = il+2
    line_atoms = lines[il_coords : il_coords+natoms]
    atom_coords = [ [ float(line.split()[1]), float(line.split()[2]), float(line.split()[3]) ] for line in line_atoms ]
    atoms       = [ str(line.split()[0]) for line in line_atoms ]
    return(atoms, atom_coords)


def read_lattice_xsf(structure):

    fin = open(structure,'r') ; lines =fin.readlines() ; fin.close()
    lat = [ [] , [] , [] ]
    for il in range(len(lines)):
        line = lines[il].split()
        if line[0] == 'PRIMVEC' :
            lat[0] = list( map(float, lines[il+1].split()[0:3] ))  
            lat[1] = list( map(float, lines[il+2].split()[0:3] ))  
            lat[2] = list( map(float, lines[il+3].split()[0:3] ))  
    return lat



def read_natom_xsf(structure):

    fin = open(structure,'r') ; lines = fin.readlines() ; fin.close()
    for il in range(len(lines)):
        line = lines[il].split()
        if line[0] =='PRIMCOORD' :
            natoms = int(lines[il+1].split()[0])
    return natoms


def write_input(structure, atoms, lattice, natom, output):
    global onsite_b
    global onsite_n
    global onsite_c

    global tcc_first
    global tbn_first
    global tcn_first
    global tcb_first

    fout = open(output+'.in','w')
    fout.write('%11s  %20s \n' %('systemlabel',output))
    fout.write('\n')
    fout.write('########### \n')
    fout.write('#%16s \n' %'on-site energies')
    fout.write('%9s \n' %'spc b n c')
    fout.write('%8s   %5.3f  \n' %('onsite b', onsite_b ))
    fout.write('%8s   %5.3f  \n' %('onsite n', onsite_n ))
    fout.write('%8s   %5.3f  \n' %('onsite c', onsite_c ))
    fout.write('%9s %1d \n' %('iopt_vpot',0))
    fout.write('\n')
    fout.write('########## \n')
    fout.write('#%30s \n' %'in-plane hopping prefactor')
    fout.write('%14s %5.3f %5.3f\n' %('intra_eppp b n',tbn_first, 0.00))
    fout.write('%14s %5.3f %5.3f\n' %('intra_eppp c c',tcc_first, 0.00))
    fout.write('%14s %5.3f %5.3f\n' %('intra_eppp c n',tcn_first, 0.00))
    fout.write('%14s %5.3f %5.3f\n' %('intra_eppp c b',tcb_first, 0.00))
    fout.write('%14s %5.3f %5.3f\n' %('intra_eppp b b',0.00, 0.00))
    fout.write('%14s %5.3f %5.3f\n' %('intra_eppp n n',0.00, 0.00))
    fout.write('\n')
    fout.write('# in-plane equilibrium distance \n')
    fout.write('intra_dmin c c %5.3f \n' %(1.42))
    fout.write('intra_dmin b n %5.3f \n' %(1.42))
    fout.write('intra_dmin c n %5.3f \n' %(1.42))
    fout.write('intra_dmin c b %5.3f \n' %(1.42))
    fout.write('\n')
    fout.write('# in-plane Qfactor \n')
    fout.write('Qfactor c c %5.3f\n' %(3.31))
    fout.write('Qfactor b n %5.3f\n' %(3.31))
    fout.write('Qfactor c b %5.3f\n' %(3.31))
    fout.write('Qfactor c n %5.3f\n' %(3.31))
    fout.write('######### \n')
    fout.write('%5s \n' %'nosym')
    fout.write('%6s %10s \n' %('kptref', 'supercell'))
    fout.write('%6s %2i \n' %('kptopt',1))
    fout.write('ngkpt 1 1 1 \n')
    fout.write('#kptbouns \n')
    fout.write('#ndivk \n')
    fout.write('\n')
    fout.write('######## \n')
    fout.write('#occupancy \n')
    fout.write('occ_iopt 1 \n')
    fout.write('occ_nelec b 0 \n')
    fout.write('occ_nelec c 1 \n')
    fout.write('occ_nelec n 2\n')
    fout.write('\n')
    fout.write('######## \n')
    fout.write('#bdos_rng -5.0 5.0 \n')
    fout.write('#dos_broadening 0.06 \n')
    fout.write('#dos_ngrid 750 \n')
    fout.write('\n')
    fout.write('######## \n')
    fout.write('#velo_iopt 1 \n')
    fout.write('#velo_nvalence 10\n')
    fout.write('#velo_nconduction 10 \n')
    fout.write('#velo_ikptselect -1 \n')
    fout.write('\n')
    fout.write('######## \n')
    fout.write('#eps_rng 1.5 6.0 \n')
    fout.write('#eps_broadening 0.080 \n')
    fout.write('#eps_ngrid 500 \n')
    fout.write('#eps_ikptselect -1 \n')
    fout.write('\n')
    fout.write('######## \n')
    fout.write('%5s  %1d \n' %('prtwf',0))
    fout.write('\n')
    fout.write('######## \n')
    fout.write('#%9s\n' %'structure')
    fout.write('%9s \n' %'monolayer')
    fout.write('%4s \n' %'cell')
    fout.write('     %12.8f   %12.8f   %12.8f \n' %tuple(lattice[0]))
    fout.write('     %12.8f   %12.8f   %12.8f \n' %tuple(lattice[1]))
    fout.write('     %12.8f   %12.8f   %12.8f \n' %tuple(lattice[2]))
    fout.write('%4s  %3d \n' %('natm',natom))
    fout.write('%5s \n' %'xcart')
    for i in range(natom):
       #print(i,atoms[i],structure[i])
        fout.write('   %1s  %10.8f   %10.8f  %10.8f\n' %(str(atoms[i]), structure[i][0], structure[i][1], structure[i][2]))
    fout.write('\n')
    fout.close()

    return()





structure = read_structure_xsf(structure_in)
lattice = read_lattice_xsf(structure_in)
natom = read_natom_xsf(structure_in)

l= len(structure_in)
output_name = structure_in[0:l-4]

write_input(structure[1], structure[0], lattice, natom, output_name)


