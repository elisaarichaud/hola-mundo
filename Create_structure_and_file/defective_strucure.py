#!/usr/bin/python3

import numpy as np
import math as m

""" This program create an armchair heterostructure
with structural defect:
It is concived to have a perfect interface besides the
defect      d_cb = d_cn =(d_cc - dbn )/2 """


##################    Input rectangular structure 2D  ######################
##Graphene
Gr_cell = np.array( [ [ 2.45999700 , 0.00000000 , 0.00000000 ] ,
                      [ 0.00000000 , 4.26083970 , 0.00000000 ] ,
                      [ 0.00000000 , 0.00000000 , 15.0000000 ] ] )
##hBN
hBN_cell = np.array( [ [ 2.5000000 , 0.00000000 , 0.00000000 ] ,
                       [ 0.0000000 , 4.33012000 , 0.00000000 ] ,
                       [ 0.0000000 , 0.00000000 , 15.0000000 ] ] )

##Crystal position structure
atms_cryst_pos = np.array( [ [ 0.16666667 , 0.83333337 , 0.000000 ] ,
                             [ 0.16666667 , 0.16666667 , 0.000000 ] ,
                             [ 0.66666667 , 0.33333333 , 0.000000 ] ,
                             [ 0.66666667 , 0.66666667 , 0.000000 ] ] )

################################################################


def create_lattice(rows) :
    global Gr_cell
    global hBN_cell
    global atms_cryst_pos

    latt = [ [] , [] , [] ]
    diff_cell = abs(Gr_cell[0,0] - hBN_cell[0,0])
    if ( sum(rows)%4 == 0 ) :
       #latt[0] = [ ( Gr_cell[0,0]*rows[0] + hBN_cell[0,0]*rows[1] )/2 , 0.000000 , 0.00000 ]
        latt[0] = [ ( Gr_cell[0,0]*rows[0] + Gr_cell[0,0]*rows[1] )/2 , 0.000000 , 0.00000 ]
    if ( rows[0]%2 != 0 ) :
       #latt[0] = [ ( Gr_cell[0,0]*rows[0] + hBN_cell[0,0]*rows[1] )/2 , 0.000000 , 0.00000 ]
        latt[0] = [ ( Gr_cell[0,0]*rows[0] + Gr_cell[0,0]*rows[1] )/2 , 0.000000 , 0.00000 ]
   #latt[1] = [ 0.000000 , (Gr_cell[1,1] + hBN_cell[1,1])*rows[2]/2 , 0.000000 ]
    latt[1] = [ 0.000000 , (Gr_cell[1,1] + Gr_cell[1,1])*rows[2]/2 , 0.000000 ]
    latt[2] = [ 0.000000 , 0.0000000 , 15.000000 ]

    return (latt)

def create_structure(rows , column , material) : 
    global Gr_cell
    global hBN_cell
    global atms_cryst_pos

    atoms_coord = [] 
    if material == 'Gr'  : 
        cell =  Gr_cell ; type_at = [ 'C' , 'C' , 'C' , 'C' ]
        for jcol in range( column ) :
            for irow in range( m.floor(rows/2) ):
                for katm in range( 4 ) :
                    atoms_coord.append( [ (irow + atms_cryst_pos[katm,0])*cell[0,0] , (jcol + atms_cryst_pos[katm,1])*cell[1,1] , 0.00000 , type_at[katm] ] )
            if (rows%2 != 0 ) :
                atoms_coord.append( [ ( m.floor(rows/2) + atms_cryst_pos[0,0] )*cell[0,0] , (jcol +atms_cryst_pos[0,1])*cell[1,1] , 0.00000 , type_at[0] ] )
                atoms_coord.append( [ ( m.floor(rows/2) + atms_cryst_pos[1,0] )*cell[0,0] , (jcol +atms_cryst_pos[1,1])*cell[1,1] , 0.00000 , type_at[1] ] )
    
    if material == 'hBN' : 
        cell = Gr_cell ; type_at = [ 'B' , 'N' , 'B' , 'N' ]
        for jcol in range( column ) :
            shift = 0.0
            if (rows%2 != 0 ) :
                shift = cell[0,0]/2
                atoms_coord.append( [  ( atms_cryst_pos[2,0] )*cell[0,0] - shift, (jcol +atms_cryst_pos[2,1])*cell[1,1] , 0.00000 , type_at[0] ] )
                atoms_coord.append( [  ( atms_cryst_pos[3,0] )*cell[0,0] - shift, (jcol +atms_cryst_pos[3,1])*cell[1,1] , 0.00000 , type_at[1] ] )
            for irow in range ( m.floor(rows/2) ) :
                for katm in range( 4 ) :
                    atoms_coord.append( [ shift + (irow + atms_cryst_pos[katm,0])*cell[0,0] , (jcol + atms_cryst_pos[katm,1])*cell[1,1] , 0.00000 , type_at[katm] ] )

    return atoms_coord


def deplace_structure( structure, deplacement ) :
    
    for i in range( len(structure) ) :
        structure[i][0] += deplacement[0]
        structure[i][1] += deplacement[1]
    
    return structure

def distance ( x1 , y1 , x2 , y2 ):
    d = m.sqrt( ( x1 - x2 ) **2 + ( y1 - y2 ) **2 )
    return d


def eliminate_double_atoms( structure_basis , structure_island ) :
    index = -1
    for j in range ( len(structure_island ) ) :
        for i in range (len(structure_basis) ) :
            dist = distance ( structure_basis[i][0] , structure_basis[i][1] , structure_island[j][0] , structure_island[j][1])
            if dist < 1.0 :         index = i
        if index != -1 :    del structure_basis[index]
        index = -1

    return structure_basis



def write_xsf_file(structure , cell  , output_name):
    global Gr_cell
    global hBN_cell


    w=open(output_name, 'w')

    w.write(' CRYSTAL \n')
    w.write(' PRIMVEC\n')
    w.write('%10.6f %10.6f %10.6f \n' % tuple(cell[0]))
    w.write('%10.6f %10.6f %10.6f \n' % tuple(cell[1]))
    w.write('%10.6f %10.6f %10.6f \n' % tuple(cell[2]))
    w.write(' PRIMCOORD \n')
    w.write('%10i %10i \n' %( len(structure) ,1 ) )
    for i in range ( len(structure) ):
        w.write(' %3s  %10.6f  %10.6f  %10.6f     #  %3i\n' % ( structure[i][3] , structure[i][0] , structure[i][1] , structure[i][2], i+1))

    w.close()


def read_xsf_file(input_name):
     
    fin = open(input_name,'r') ; lines = fin.readlines() ; fin.close()
    for il in range( len(lines) ) :
        if( lines[il].split()[0] == 'PRIMCOORD' ) : 
            natm = int( lines[il+1].split()[0] )
            lines_atomic_position =  lines[il+2 : il+2+natm]
            atoms_coord = [ [ float( line.split()[1] ) , float( line.split()[2] ) , float( line.split()[3] ) , line.split()[0] ] for line in lines_atomic_position ]
    return (atoms_coord)



#########################################################################################



rows   = ( input( '### Gr rows         hBN rows       columns     :   ') ).split()
for i in range( len(rows) ):
    rows[i] = int(rows[i])
defect = input( '### defect xsf file     x and y position of the defect with respect to left bottom atom:')
if ( defect != '' ) :
     dfct = True
     defect_structure = defect.split()[0]
     defect_position = [ float(defect.split()[1]) , float(defect.split()[2]) ]
else :
     dfct = False
  

### create cell and structure

cell = create_lattice(rows)

structure_Gr  = create_structure(rows[0],rows[2],'Gr')
structure_hBN = create_structure(rows[1],rows[2],'hBN')

### deplace hBN ribbon
diff = abs(Gr_cell[0,0]-hBN_cell[0,0])/4
deplacement = [rows[0]/2*Gr_cell[0,0]+diff , 0.000 ]

structure_hBN = deplace_structure( structure_hBN, deplacement)
structure = structure_Gr+structure_hBN

### introduce defect
structure_defect = read_xsf_file( defect_structure )

structure_defect = deplace_structure( structure_defect , defect_position )

### eliminate repeted atoms
structure = eliminate_double_atoms(structure , structure_defect )

structure += structure_defect


write_xsf_file(structure, cell, 'toto.xsf')



## structure_defect

