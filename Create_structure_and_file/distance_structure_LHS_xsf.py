#!/usr/bin/python3


import numpy as np
import sys
from math import sqrt
import copy

#file_in = input('#### xsf Structure file  :      ')

fin= 'structure.19.xsf'

def distance(atom1, atom2):
    
    d = 0
    for i in range(3):
        d += ( atom1[i] - atom2[i] ) **2
    d = sqrt(d)
    return(d)


def read_natom(file_str):

    fin = open( file_str, 'r' )
    lines = fin.readlines()
    for i in range(len(lines)):
        if lines[i].split()[0] == 'PRIMCOORD':
              natom = lines[i+1].split()[0]
    return (int(natom))


def read_lattice_parameter(file_str):
    fin = open( file_str, 'r' )
    lines = fin.readlines()
    lattice=np.zeros([3])
    for i in range(len(lines)):
        if lines[i].split()[0] == 'PRIMVEC':
              lattice[0] = float(lines[i+1].split()[0])
              lattice[1] = float(lines[i+2].split()[1])
              lattice[2] = float(lines[i+3].split()[2])
    return(lattice)


def read_structure(file_str,natom):
    
    fin = open( file_str, 'r' )
    lines = fin.readlines()
    fin.close()
    structure=np.zeros([natom,3])
    atoms    =np.zeros([natom],str)
    for i in range(len(lines)):
        if lines[i].split()[0] == 'PRIMCOORD':
           ln = i+2
    for i in range(ln,ln+natom):
        atoms[i-ln] = lines[i].split()[0]
        for j in range(3):
            structure[i-ln,j] = float(lines[i].split()[j+1]) 
    return(atoms,structure)



def write_output(structure,natom,atoms,lattice):

    fout = open('distance.dat' ,'w')
    fout.write('##%45si \n' %'Study of distance between first neighbours')
    fout.write('##%16s        %20s         %20s\n'  %('vertical bonds', 'horizontal bonds up' ,'horizontal bonds down') )
    structure_aux=copy.deepcopy(structure)
    for i in range(int(natom/2)-1):
        if (structure[2*i,1] > 2.6 ):
             structure_aux[2*i+1,1]=lattice[1]+structure[2*i+1,1]
             fout.write('%6s  %8.6f      %6s  %8.6f    %6s  %8.6f\n' %(atoms[2*i]+str(i+1)+atoms[2*i+1]+str(i+1), distance(structure[2*i,:],structure_aux[2*i+1,:]),atoms[2*i]+str(i+1)+atoms[2*i+3]+str(i+2), distance(structure[2*i,:],structure[2*i+3,:]),atoms[2*i+1]+str(i+1)+atoms[2*i+2]+str(i+2), distance(structure[2*i+1,:],structure[2*i+2,:])))
        else: 
             fout.write('%6s  %8.6f      %6s  %8.6f    %6s  %8.6f\n' %(atoms[2*i]+str(i+1)+atoms[2*i+1]+str(i+1), distance(structure[2*i,:],structure[2*i+1,:]),atoms[2*i+1]+str(i+1)+atoms[2*i+2]+str(i+2), distance(structure[2*i+1,:],structure[2*i+2,:]),atoms[2*i]+str(i+1)+atoms[2*i+3]+str(i+2), distance(structure[2*i,:],structure[2*i+3,:])))
    ln=int(natom/2)-1     
    structure_aux[0,0]=lattice[0]+structure[0,0]
    structure_aux[1,1]=lattice[1]-structure_aux[1,1]
    structure_aux[1,0]=lattice[0]+structure[1,0]
    fout.write('%6s  %8.6f      %6s  %8.6f    %6s  %8.6f\n' %(atoms[2*ln]+str(ln+1)+atoms[2*ln+1]+str(ln+1), distance(structure[2*ln,:],structure[2*ln+1,:]),atoms[2*ln+1]+str(ln+1)+atoms[0]+str(1), distance(structure[2*ln+1,:],structure_aux[0,:]),atoms[2*ln]+str(ln+1)+atoms[1]+str(1), distance(structure[2*ln,:],structure_aux[1,:])))


    fout.close()
###corregir distance between replicas (add lattice)
###add the last column
###correct indixes


natom=read_natom(fin)

lattice=read_lattice_parameter(fin)
structure_read=(read_structure(fin,natom))
structure=structure_read[1]
atoms=structure_read[0]

write_output(structure,natom,atoms,lattice)
print(distance(structure[1,:],structure[2,:]))





