#!/usr/bin/python3

import numpy as np
import math as m


def Remove_Empty_Lines(lines) :
    new_lines = [ line for line in lines if len(line.split()) > 0 ]
    return new_lines

def decay_function(Qfactor , x ) :
    return m.exp( Qfactor*x )


def computeSK_Trambly(vSK , xcos ) :
    xcos2 =  [ xcos[0]**2 , xcos[1]**2 , xcos[2]**2 ]
    betaSK=xcos2[2]*vSK[1] + (1-xcos2[2])*vSK[0]
    return betaSK



class  Matrix_elementsDB():
    """
    Class to read the matrix elements from an input twisted TB
    with xcart coordinates

    """

    def __init__(self, filename ) :
        self.filename = filename
        self.readDB()


    def readDB(self) :
        """
        Read all the structure variables
        """
        self.readDB_nspc()
        self.readDB_spc()
        self.readDB_onsite()
        self.readDB_intra_eppp()
        self.readDB_intra_dmin()
        self.readDB_intra_Qfactor()



    def readDB_nspc(self) :
        """
        Read n-species
        """

        fin = open(self.filename ,'r') ; lines = fin.readlines() ; fin.close
        lines = Remove_Empty_Lines(lines)

        self.nspc = 0
        for il in lines :
            if il.split()[0] == 'spc' : self.nspc = len( il.split() ) - 1

        return self.nspc

    def readDB_spc(self) :
        """
        Read type species
        """

        fin = open(self.filename ,'r') ; lines = fin.readlines() ; fin.close
        lines = Remove_Empty_Lines(lines)

        self.spc = []
        for il in lines :
            if il.split()[0] == 'spc' :
                for n in range( self.nspc ) :
                    self.spc.append(il.split()[ n+1 ])
        print(self.spc)
        return self.spc


    def readDB_onsite(self) :
        """
        Read all the on-site parameters
        """

        fin = open(self.filename , 'r' ) ; lines = fin.readlines() ; fin.close
        lines = Remove_Empty_Lines(lines)

        onsite = np.zeros( [self.nspc] )
        for il in lines :
            for ns in range(self.nspc) :
                if ( il.split()[0] == 'onsite' and il.split()[1] == self.spc[ns].upper() ) :  onsite[ns] =  il.split()[2]  
                if ( il.split()[0] == 'onsite' and il.split()[1] == self.spc[ns].lower() ) :  onsite[ns] =  il.split()[2]  
                
        
        self.onsite = onsite
        return self.onsite


    def readDB_intra_eppp(self) :
        """
        Read all the intra_eppp parameters
        """

        fin = open(self.filename , 'r' ) ; lines = fin.readlines() ; fin.close
        lines = Remove_Empty_Lines(lines)

        intra_eppp = np.zeros( [self.nspc , self.nspc , 2 ] )
        for il in lines : 
            for ns1 in range(self.nspc) :
                for ns2 in range(self.nspc) :
                    if ( il.split()[0] == 'intra_eppp' and il.split()[1]==self.spc[ns1].lower() and il.split()[2]==self.spc[ns2].lower() ) :
                        intra_eppp[ns1,ns2,0] = float(il.split()[3])   
                        intra_eppp[ns2,ns1,0] = float(il.split()[3])   
                        intra_eppp[ns1,ns2,1] = float(il.split()[4])   
                        intra_eppp[ns2,ns1,1] = float(il.split()[4])   

                    if ( il.split()[0] == 'intra_eppp' and il.split()[1]==self.spc[ns1].upper() and il.split()[2]==self.spc[ns2].upper() ) :
                        intra_eppp[ns1,ns2,0] = float(il.split()[3])   
                        intra_eppp[ns2,ns1,0] = float(il.split()[3])   
                        intra_eppp[ns1,ns2,1] = float(il.split()[4])   
                        intra_eppp[ns2,ns1,1] = float(il.split()[4])   

        self.intra_eppp = intra_eppp
        return self.intra_eppp


    def readDB_intra_dmin(self) :
        """
        Read all the intra_dmin parameters
        """

        fin = open(self.filename , 'r' ) ; lines = fin.readlines() ; fin.close
        lines = Remove_Empty_Lines(lines)

        intra_dmin = np.zeros( [self.nspc , self.nspc ] )
        for il in lines : 
            for ns1 in range(self.nspc) :
                for ns2 in range(self.nspc) :
                    if ( il.split()[0] == 'intra_dmin' and il.split()[1]==self.spc[ns1].lower() and il.split()[2]==self.spc[ns2].lower() ) : 
                        intra_dmin[ns1,ns2] = float(il.split()[3]) 
                        intra_dmin[ns2,ns1] = float(il.split()[3]) 
                    if ( il.split()[0] == 'intra_dmin' and il.split()[1]==self.spc[ns1].upper() and il.split()[2]==self.spc[ns2].upper() ) : 
                        intra_dmin[ns1,ns2] = float(il.split()[3]) 
                        intra_dmin[ns2,ns1] = float(il.split()[3]) 

        self.intra_dmin = intra_dmin
        return self.intra_dmin


    def readDB_intra_Qfactor(self) :
        """
        Read all the intra_Q factor parameters
        """

        fin = open(self.filename , 'r' ) ; lines = fin.readlines() ; fin.close
        lines = Remove_Empty_Lines(lines)

        intra_Qfactor = np.zeros( [ self.nspc , self.nspc ] )
        for il in lines : 
            for ns1 in range(self.nspc) :
                for ns2 in range(self.nspc) :
                    if ( il.split()[0] == 'intra_Qfactor' and il.split()[1]==self.spc[ns1].lower() and il.split()[2]==self.spc[ns2].lower() ) : 
                        intra_Qfactor[ns1,ns2] =  float(il.split()[3]) 
                        intra_Qfactor[ns2,ns1] =  float(il.split()[3]) 
                    if ( il.split()[0] == 'intra_Qfactor' and il.split()[1]==self.spc[ns1].upper() and il.split()[2]==self.spc[ns2].upper() ) : 
                        intra_Qfactor[ns1,ns2] =  float(il.split()[3]) 
                        intra_Qfactor[ns2,ns1] =  float(il.split()[3]) 
        
        self.intra_Qfactor = intra_Qfactor
        return self.intra_Qfactor

    
    def readDB_type_dependence(self) : 
        """
        Read the type of dependence for intra_eppp
        """

        fin = open(self.filename , 'r' ) ; lines = fin.readlines() ; fin.close
        lines = Remove_Empty_Lines(lines)

        self.intra_eppp_dependence = ""
        for il in lines : 
            if ( il.split()[0] == 'intra_eppp_dependence' ) : self.intra_eppp_dependence = il.split()[1] 
        
        return self.intra_eppp_dependence
        
     #################################################################
     ### END of readDB  ###
     #################################################################



