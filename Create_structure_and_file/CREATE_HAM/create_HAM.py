#!/usr/bin/python3

from matrix_elementsdb import *
from structuredb import *


""" 
This program create a HAM file where the Hamiltonian
where intra_eppp depends on the distance
"""

def distance(xcart_iatom , xcart_jatom, shift_jatom) :
    dist=0.0 

    for i in range(3):
        dist+= (xcart_iatom[i]-xcart_jatom[i]-shift_jatom[i])**2
    distance = m.sqrt(dist)
    return distance

def cos_directions(xcart_iatom, xcart_jatom) :
    dx = xcart_iatom[0] - xcart_jatom[0]
    dy = xcart_iatom[1] - xcart_jatom[1]
    dz = xcart_iatom[2] - xcart_jatom[2]
    dist = m.sqrt(dx**2 + dy**2 + dz**2)
    coscart = [ dx/dist , dy/dist , dz/dist ]
    return coscart


##########################################################

file_in = input( " ###    Input file   : " )


structure = StructureDB( file_in ) 
matrix    = Matrix_elementsDB( file_in )

structure.intra_3_first_neighbours(mnnei=15, ctf_intra=2.6, mnnei_fin=3)


wout = open( structure.label.lower() + '_HAM.in' , 'w' ) 
wout.write( '# column     data \n' )
wout.write( '# 1-2        inei, iorb_b \n' )
wout.write( '# 3          hopping \n' )
wout.write( '# 4-5        cell shift \n' )
wout.write( '# 6          1st/2nd neighbour, -1 --> interlayer \n' )

for iatom in range( structure.natom ) :
    wout.write( ' iorb=  %5i nnei=  %2i onsite+V(r)=   %12.8f \n' %(iatom+1 , structure.nnei[iatom] , matrix.onsite[ structure.xtype[iatom] ] ) )
    for jatom in range( structure.nnei[iatom] ) :
        #structure parameters
        xcart_iatom = structure.xcart[iatom] 
        xcart_jatom = structure.xcart[ structure.neiindx[iatom,jatom] ]
        ishiftnei_jatom = structure.ishiftnei[iatom,jatom]
        shift_jatom = [ [] , [] , [] ]
        for k in range(3) :
            shift_jatom[k] = ishiftnei_jatom[0]*structure.cell[0][k] + ishiftnei_jatom[1]*structure.cell[1][k] + ishiftnei_jatom[2]*structure.cell[2][k]
        dist = distance(xcart_iatom,xcart_jatom, shift_jatom)
        coscart = cos_directions(xcart_iatom , xcart_jatom)
        #intra_epp calculation
        prefactor = matrix.intra_eppp[ structure.xtype[iatom] , structure.xtype[structure.neiindx[iatom,jatom]] , 0 ]
        intra_dmin = matrix.intra_dmin[ structure.xtype[iatom] , structure.xtype[structure.neiindx[iatom,jatom]] ]
        intra_Qfactor = matrix.intra_Qfactor[ structure.xtype[iatom] , structure.xtype[structure.neiindx[iatom,jatom]] ]
      # if (iatom < 10 ):
      #     print(iatom, jatom , dist, prefactor, structure.xtype[structure.neiindx[iatom,jatom]], intra_dmin, intra_Qfactor)

     #  print(prefactor , intra_dmin , intra_Qfactor, dist , structure.xtype[iatom])
  
        vSK = [ prefactor * decay_function(intra_Qfactor, ( intra_dmin-dist )) , 0.00 ]
        betaSK = computeSK_Trambly(vSK, coscart)

        wout.write( ' %2i    %3i   %14.12f  %2i  %2i  %2i \n' %(jatom+1 , structure.neiindx[iatom, jatom]+1 , betaSK , structure.ishiftnei[iatom , jatom][0] , structure.ishiftnei[iatom, jatom][1] , 1 ))


