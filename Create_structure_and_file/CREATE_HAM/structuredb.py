#!/usr/bin/python3


import numpy as np
import math as m


def Remove_Empty_Lines(lines) :
    new_lines = [ line for line in lines if len(line.split()) > 0 ]
    return new_lines



class  StructureDB():
    """
    Class to read the structure from an input twisted TB 
    with xcart coordinates

    """

    def __init__(self, filename ) :
        self.filename = filename
        self.readDB()


    def readDB(self) :
        """
        Read all the structure variables
        """
 
        self.readDB_label()
        self.readDB_cell()
        self.readDB_natom()
        self.readDB_nspc()
        self.readDB_spc()
        self.readDB_xcart()
        self.readDB_xtype()
        self.readDB_layer()
       #self.inter_3_first_neighbours

    def readDB_label(self) :
        """
        Read systemlabel output
        """

        fin = open(self.filename ,'r') ; lines = fin.readlines() ; fin.close 
        lines = Remove_Empty_Lines(lines)

        self.label = ""
        for il in lines :
            if il.split()[0] == 'systemlabel' : self.label =  il.split()[1] 

        return self.label

    def readDB_cell(self) :
        """
        Read cell vectors
        """

        fin = open(self.filename ,'r') ; lines = fin.readlines() ; fin.close 
        lines = Remove_Empty_Lines(lines)

        self.cell = [ [] , [] , [] ]
        for il in range( len(lines) ) :
            if ( lines[il].split()[0] == 'cell' ) :
                self.cell[0] = [float( lines[il+1].split()[0]), float( lines[il+1].split()[1]), float( lines[il+1].split()[2]) ] 
                self.cell[1] = [float( lines[il+2].split()[0]), float( lines[il+2].split()[1]), float( lines[il+2].split()[2]) ] 
                self.cell[2] = [float( lines[il+3].split()[0]), float( lines[il+3].split()[1]), float( lines[il+3].split()[2]) ] 
               #self.cell[0] = (float( lines[il+1].split()[i] ) for i in range(3)  )
               #self.cell[1] = (float( lines[il+2].split()[i] ) for i in range(3)  )
               #self.cell[2] = (float( lines[il+3].split()[i] ) for i in range(3)  )

        return self.cell


    def readDB_natom(self) :
        """
        Read natoms 
        """

        fin = open(self.filename ,'r') ; lines = fin.readlines() ; fin.close 
        lines = Remove_Empty_Lines(lines)

        self.natom = 0
        for il in lines :
            if il.split()[0] == 'natm' : self.natom = int( il.split()[1] )
        
        return self.natom

   
    def readDB_nspc(self) :
        """
        Read n-species
        """

        fin = open(self.filename ,'r') ; lines = fin.readlines() ; fin.close 
        lines = Remove_Empty_Lines(lines)

        self.nspc = 0
        for il in lines :
            if il.split()[0] == 'spc' : self.nspc = len( il.split() ) - 1

        return self.nspc

    def readDB_spc(self) :
        """
        Read type species
        """

        fin = open(self.filename ,'r') ; lines = fin.readlines() ; fin.close 
        lines = Remove_Empty_Lines(lines)

        self.spc = []
        for il in lines :
            if il.split()[0] == 'spc' : 
                for n in range( self.nspc ) :
                    self.spc.append(il.split()[ n+1 ])

        return self.spc

    def readDB_xcart(self) :
        """
        Read only xcart positions
        """

        fin = open(self.filename ,'r') ; lines = fin.readlines() ; fin.close 
        lines = Remove_Empty_Lines(lines)

        self.xcart = []
        for il in range ( len(lines) ) :
            if lines[il].split()[0] == 'xred' : print( 'StructureDb only read xcart positions' )
            if lines[il].split()[0] == 'xcart' :
               self.xcart = [ [ float(lines[il+1+n].split()[1]) , float(lines[il+1+n].split()[2]) , float(lines[il+1+n].split()[3]) ] for n in range (self.natom) ]
    
        return self.xcart

    def readDB_xtype(self) :
        """
        Read specie of each atom  positions
        """

        fin = open(self.filename ,'r') ; lines = fin.readlines() ; fin.close 
        lines = Remove_Empty_Lines(lines)

        self.xtype = []
        for il in range ( len(lines) ) :
            if ( lines[il].split()[0] == 'xcart' ) :
               for iatom in range(1, self.natom+1 ) :
                   for ns1 in range( self.nspc ) :
                       if ( lines[il+iatom].split()[0].lower() == self.spc[ns1].lower() ) :
                           self.xtype += [ ns1 ]
        return self.xtype

        
    def readDB_layer(self) :
        """
        Read monolayer or bilayer
        """

        fin = open(self.filename ,'r') ; lines = fin.readlines() ; fin.close 
        lines = Remove_Empty_Lines(lines)

        self.monolayer = True
        for il in range ( len(lines) ) :
            if lines[il].split()[0] == 'bilayer' : self.monolayer = False

        return self.monolayer

##################################################################

### END of ReadDB  ###

##################################################################


    def how_many_cells(self, ctf_intra= 2.6) :
        """
        This function give the number of cells needed
        to recolect all the neighbours
        """
        cell = self.cell

        ncell = [ 0 , 0 , 0 ]
        for i in range(3) :
            D = m.sqrt( ( cell[i][0]**2+cell[i][1]**2+cell[i][2]**2 ) )
            ncell[i] = int(ctf_intra/D)+1 
        return ncell


    def intra_3_first_neighbours(self, mnnei = 9, ctf_intra=2.6, mnnei_fin=3) :
        """
        This function obtain the first three neighbours
        of each atom
        """

        natom = self.natom
        xcart = self.xcart
        cell = self.cell
        nnei = -1*np.ones( [natom], dtype=int )
        neidistance = 100*np.ones( [natom, mnnei ] )
        neiindx = -1*np.ones( [natom, mnnei ] , dtype=int )
        ishiftnei = -100*np.ones( [natom, mnnei , 3 ] , dtype=int )

        ncell = self.how_many_cells(ctf_intra=3.0)

        for iatom in range(natom) :
            for jatom in range(natom) :

                for i1 in range( -ncell[0] , ncell[0]+1 ) :
                    for i2 in range( -ncell[1] , ncell[1]+1 ) :
                        for i3 in range( -ncell[2] ,  ncell[2]+1 ) :

                            icell = [ i1, i2 , i3 ]
                            shift = [ [] , [] , [] ]
                            xcart_j = [ [] , []  , []  ]
                            dist = 0.0
                            for j in range(3) :
                                shift[j] = i1*cell[0][j] + i2*cell[1][j] +i3*cell[2][j]
                                xcart_j[j] = xcart[jatom][j] + shift[j]
                                dist += ( xcart_j[j] - xcart[iatom][j] )**2
                                
                            if ( dist <= ctf_intra**2 ) :
                                if( i1!=0 or i2!=0 or i3!=0 or jatom!=iatom ) :
                                    nnei[iatom] += 1
                                    neidistance[iatom, int(nnei[iatom]) ] = dist
                                    neiindx[iatom, int(nnei[iatom]) ] = jatom
                                    for k in range(3) :
                                        ishiftnei[iatom , int(nnei[iatom]), k ] = icell[k]
            for jnei in range(0, int(nnei[iatom])+1 ):
                for knei in range( jnei ) :
                    if ( neidistance[iatom,jnei] < neidistance[iatom,knei] or neidistance[iatom,knei]==100 ) :
                        neidistance_higher = neidistance[iatom,knei]
                        neidistance_lower  = neidistance[iatom,jnei]
                        neiindx_higher = neiindx[iatom,knei]
                        neiindx_lower = neiindx[iatom,jnei]

                        neidistance[iatom,knei] = neidistance_lower
                        neidistance[iatom,jnei] = neidistance_higher
                        neiindx[iatom,knei] = neiindx_lower
                        neiindx[iatom,jnei] = neiindx_higher
 
                        for k in range(3) :
                            ishiftnei_lower = ishiftnei[iatom,jnei,k]
                            ishiftnei_higher = ishiftnei[iatom,knei,k]

                            ishiftnei[iatom,knei,k] = ishiftnei_lower
                            ishiftnei[iatom,jnei,k] = ishiftnei_higher
                      

                    nnei[iatom] = mnnei_fin
            self.nnei = nnei
            self.neiindx = neiindx
            self.ishiftnei = ishiftnei
            


                                



 

