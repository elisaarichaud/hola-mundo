#!/usr/bin/python3


import numpy as np
from math import acos
from math import cos
from math import sqrt
from math import ceil
from math import floor

#Cosas que mejorar
#pasivation hBN
#que te diga el numero de atomos y el tamano de la celula
#xsf para vesta


import numpy as np
from math import ceil




############# INPUT


### Input structure file (txt file from vesta)
##Gr rectangular cell ###
Gr_coord = np.array([[2.8405592, 0.0000000,  0.000000],
                    [0.0000000, 0.0000000,  0.000000],
                    [0.7101394, 1.2299978,  0.000000],
                    [2.1304199, 1.2299978,  0.000000]])

Gr_cell  = np.array([4.2608397,2.459997,0.0])
Gr_atoms = [ 'C' , 'C' , 'C' , 'C' ]

##hBN rectangular cell ###
hBN_coord = np.array([[2.8867472, 0.0000000, 0.00000000],
                     [0.0000000, 0.0000000, 0.00000000],
                     [0.7216872, 1.2499982, 0.00000000],
                     [2.1650600, 1.2499982, 0.00000000]])

hBN_cell = np.array([4.330120,2.499996,0.0])
hBN_atoms = [ 'B' , 'N' , 'B' , 'N' ]



#rows_in = input('##### Material and rows to compose the LH   :')
material = [  'Gr'  ,  'hBN'  ] 
row     = [ 10  , 8  ] 

def obtain_cell(material):
    global  Gr_cell
    global  hBN_cell

    if   material == 'Gr'   : cell = Gr_cell
    elif material == 'hBN'  : cell = hBN_cell
    else : print('The material cell is not in the Script')
    return cell

def obtain_coord(material):
    global  Gr_coord
    global  hBN_coord

    if   material == 'Gr'   : coord = Gr_coord
    elif material == 'hBN'  : coord = hBN_coord
    else : print('The material coord is not in the Script')
    return coord

def obtain_atoms(material):
    global  Gr_atoms
    global  hBN_atoms

    if   material == 'Gr'   : atoms = Gr_atoms
    elif material == 'hBN'  : atoms = hBN_atoms
    else : print('The material atoms is not in the Script')
    return atoms

def create_lattice(row , material):
    global  Gr_cell
    global  hBN_cell
 
    
    cell_1 = obtain_cell(material[0])
    cell_2 = obtain_cell(material[1])
    print(cell_2)
    latt = [ [] , [] , [] ]
    if (row[0]%2 == 0):
        latt[0] = [(cell_1[0]*row[0]+cell_2[0]*row[1]) /2 , 0.00000000 , 0.00000000]
    latt[1] = [ 0.00000000, ( cell_1[1] + cell_2[1] ) /2 , 0.00000000]
    latt[2] = [ 0.00000000, 0.00000000, 15.00000000]

    return (latt)


def create_structure(row , material):
    global Gr_coord
    global Gr_cell
    global hBN_coord
    global hBN_cell

    structure = np.zeros([sum(row)*3,3])
    type_atoms = []
    coord_1 = obtain_coord(material[0] )
    cell_1  = obtain_cell(material[0] )
    atoms_1 = obtain_atoms(material[0] )
    coord_2 = obtain_coord(material[1]) 
    cell_2  = obtain_cell(material[1]) 
    atoms_2 = obtain_atoms(material[1])


    for i in range(int(row[0]/2)):
        structure[i*4]   = coord_1[0] + [cell_1[0]*i, 0.00000000, 0.00000000 ]
        type_atoms.append(atoms_1[0])
        structure[i*4+1] = coord_1[1] + [cell_1[0]*i, 0.00000000, 0.00000000 ]
        type_atoms.append(atoms_1[1])
        structure[i*4+2] = coord_1[2] + [cell_1[0]*i, 0.00000000, 0.00000000 ]
        type_atoms.append(atoms_1[2])
        structure[i*4+3] = coord_1[3] + [cell_1[0]*i, 0.00000000, 0.00000000 ]
        type_atoms.append(atoms_1[3])
    
    size_aux = cell_1[0]*(row[0]/2-1/2)+cell_2[0]*(1/2)
    natom_aux = (int(row[0]/2))*4
    for i in range(floor(row[1]/2)):
         structure[natom_aux+i*4]   = coord_2[0] + [size_aux+cell_2[0]*i, 0.00000000, 0.00000000 ]
         type_atoms.append(atoms_2[0])
         structure[natom_aux+i*4+1] = coord_2[1] + [size_aux+cell_2[0]*i, 0.00000000, 0.00000000 ]
         type_atoms.append(atoms_2[1])
         structure[natom_aux+i*4+2] = coord_2[2] + [size_aux+cell_2[0]*i, 0.00000000, 0.00000000 ]
         type_atoms.append(atoms_2[2])
         structure[natom_aux+i*4+3] = coord_2[3] + [size_aux+cell_2[0]*i, 0.00000000, 0.00000000 ]
         type_atoms.append(atoms_2[3])
  
  
  
    return (structure,type_atoms)
  


def write_xsf_file(structure,acell,atoms):
    global row

    name_xsf = 'zigzag_'+material[0]+'-'+str(row[0])+'_'+material[1]+'-'+str(row[1])+'_0.xsf'
    w=open(name_xsf, 'w')

    w.write(' CRYSTAL \n')
    w.write(' PRIMVEC\n')
    w.write('%10.6f %10.6f %10.6f \n' % tuple(latt[0]))
    w.write('%10.6f %10.6f %10.6f \n' % tuple(latt[1]))
    w.write('%10.6f %10.6f %10.6f \n' % tuple(latt[2]))
    w.write(' PRIMCOORD \n')
    w.write('%10i %10i \n' %(sum(row)*2,1))
    for i in range (sum(row)*2):
        w.write(' %3s  %10.6f  %10.6f  %10.6f     #  %3i\n' % ( atoms[i], structure[i,0], structure[i,1], structure[i,2], i+1))
        
    w.close()



latt = create_lattice(row , material)
structure = create_structure(row , material)
atoms = structure[1]
coordinates = structure[0]

write_xsf_file(coordinates,latt,atoms)

   
