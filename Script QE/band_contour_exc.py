#!/usr/bin/python3
import numpy as np
from math import sqrt


def Remove_comments(lines):
    new_lines = [ line for line in lines if line[0] != '#' ]
    return new_lines

def Remain_comments(lines):
    new_lines = [ line for line in lines if line[0] == '#' ]
    return new_lines

def Remove_Empty_Lines(lines) :
    new_lines = [ line for line in lines if len(line.split()) > 0 ]
    return new_lines



def obtain_K_point( file_in):
    fin = open(file_in , 'r' ) ; lines=fin.readlines() ; fin.close()
    lines =  Remove_comments(lines)
    lines =  Remove_Empty_Lines(lines)

    K = np.zeros( [len(lines) ,2 ] )
    for i in range( len( lines) ) :
        K[i,:] = [ float(lines[i].split()[0]) , float(lines[i].split()[1]) ]

    return K


def obtain_weight( file_in ):
    fin = open(file_in , 'r' ) ; lines=fin.readlines() ; fin.close()
    lines =  Remove_comments(lines)
    lines =  Remove_Empty_Lines(lines)
   
    weight = np.zeros( [len(lines) , 4 ] )
    weight = [ [ float(line.split()[2]) , float(line.split()[3]) ,float(line.split()[4]) , float(line.split()[5]) ] for line in lines ]
    return weight


def module( v1, v2 ) :
    mod =sqrt( (v1[0]-v2[0])**2 + (v1[1]-v2[1])**2 )
    return mod


def compare_K_points( K , file_energy ):
    fin = open( file_energy , 'r' ) ; lines=fin.readlines()  ; fin.close()
    lines = Remove_comments(lines)
    lines =  Remove_Empty_Lines(lines)

    k_lines= np.zeros([len(K)])
    for i in range( len(K) ) :
        mod_min = 0.5
        j_min=0
        for j in range( len( lines) ) :
            K_c = [ float(lines[j].split()[0]) , float(lines[j].split()[1]) ]
            mod_K = module( K[i,:] , K_c )
            if ( mod_K < mod_min ) : mod_min = mod_K ; j_min = j
        k_lines[i] = j_min
    return(k_lines)

def obtain_contour_energy( K_lines , file_energy , K ) :
    fin = open( file_energy , 'r' ) ; lines=fin.readlines()  ; fin.close()
    lines = Remove_comments(lines)
    lines =  Remove_Empty_Lines(lines)

    energy =np.zeros([len(K_lines)])
    for i in range(len(K_lines) ) :
        energy[i] = float ( lines[ int(K_lines[i] ) ].split()[2] )
    
    for i in range(1,len(K_lines) ) :
       if (K[i,0] ==-K[i-1,0] and K[i,1] == -K[i-1,1] ) : energy[i] = energy[i-1]
    return energy


def write_output( K , file_in , weight, energy1 , energy2 , energy3 , energy4):
    fin = open( file_in , 'r' ) ; lines = fin.readlines() ; fin.close()
    lines = Remain_comments(lines)
    
    fout = open( file_in , 'w' )
    fout.write(lines[0])
    fout.write(lines[1])
    for i in range (len(K) ) :
       fout.write( ' %8.6f   %8.6f     %8.6f  %8.6f    %8.6f  %8.6f    %8.6f  %8.6f    %8.6f  %8.6f \n' %(K[i,0] , K[i,1], energy1[i], weight[i][0] , energy2[i], weight[i][1] , energy3[i], weight[i][2] , energy4[i], weight[i][3] ) )
    fout.write('\n')
    for j in range(2,len(lines)):
        fout.write(lines[j])
    fout.close()
   




file_in = input( "#### File exc weight  : ")
bands_in= input( "#### Bands            : ")
bands = [ int(band) for band in bands_in.split() ]


K = obtain_K_point(file_in)
K_lines = compare_K_points( K , '../contour_band39.dat')
weight = obtain_weight( file_in )

energy = [ [] , [] , [] , [] ]
for i in range(len(bands)):
    energy[i] = obtain_contour_energy( K_lines, '../contour_band'+str(bands[i])+'.dat' , K )

write_output( K , file_in , weight , energy[0] , energy[1] , energy[2] ,  energy[3] )







  











