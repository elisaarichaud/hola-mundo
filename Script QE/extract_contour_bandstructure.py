#!/usr/bin/python3

import numpy as np
import matplotlib.pyplot as plt
from math import ceil


#f_bands = input( '### Bands contour input :   ')

def Remove_Empty_Lines(lines) :
    new_lines = [ line for line in lines if len(line.split()) > 0 ]
    return new_lines



def obtain_K_mesh(f_band) :
    fin = open(f_band+'.in' , 'r' )   ; lines = fin.readlines()  ;  fin.close()
    k_cell   = [ [] , [] , [] ]
    k_weight = []
    for i in range( len(lines) ) :
        if (lines[i].split()[0] == 'K_POINTS' ):
            k_cell   = [ [ float(line.split()[0]) , float(line.split()[1]) , float(line.split()[2]) ] for line in lines[i+2:i+5] ]
            k_weight = [ int(line.split()[3]) for line in lines[i+2:i+5]]
    kx = np.linspace( k_cell[0][0] , k_cell[1][0] , k_weight[1] )
    ky = np.linspace( k_cell[0][1] , k_cell[2][1] , k_weight[2] )

    Kx , Ky = np.meshgrid( kx, ky)
    return(Kx , Ky)


#K=obtain_K_mesh('pw.bands_contour')
#Kx=K[0]
#Ky=K[1]

def obtain_nK(f_band) :
    fin = open(f_band+'.out' , 'r' )   ; lines = fin.readlines()  ;  fin.close()
    for line in lines : 
        if ( line.split('=')[0]  == '     number of k points' ) : nk = line.split('=')[1]
    return(nk)


def obtain_nb(f_band) :
    fin = open(f_band+'.out' , 'r' )   ; lines = fin.readlines()  ;  fin.close()
    for line in lines : 
        if ( line.split('=')[0]  == '     number of Kohn-Sham states' ) : nb = line.split('=')[1]
    return(nb)


def obtains_K_output(f_band,nk) : 
    fin = open(f_band+'.out' , 'r' )   ; lines = fin.readlines()  ;  fin.close()
    lines = Remove_Empty_Lines( lines )
    k_points = np.zeros([nk,2])
    for i in range( len( lines ) ) :
        if ( ( lines[i].split()[0] == 'cryst.') and ( lines[i].split()[1] == 'coord.' ) ) :
            lines_k = lines[ i+1 : i+nk+1]
            kx_points = [ float(line.split()[4])  for line in lines_k ]
            ky_points = [ float(line.split()[5])  for line in lines_k ]
    for k in range(nk):
        if (kx_points[ k ] > 0.5 ) : kx_points[ k ] -= 1.0
    return ( kx_points ,  ky_points ) 


def obtain_energy_output( f_band, nk ,nb, bands ):
    fin = open(f_band+'.out' , 'r' )   ; lines = fin.readlines()  ;  fin.close()
    lines = Remove_Empty_Lines( lines )
    for i in range( len( lines ) ) :
        if ( ( lines[i].split()[0] == 'End' ) ):
            lines_en = lines[ i+1 : i+(1+ceil(nb/8))*nk +1]

    energy = np.zeros([nk, len(bands) ])
    for k in range(nk) :
        for b in range( len(bands) ) : 
            if ( bands[b]%8 == 0 ): energy[ k , b ] = float(lines_en[ (1+ceil(nb/8))*k + ceil(bands[b]/8) ].split()[ 7 ])
            else :   energy[ k , b ] = float(lines_en[ (1+ceil(nb/8))*k + ceil(bands[b]/8) ].split()[ bands[b]%8 - 1 ] )
    return(energy)



def write_output(k_point , energy , band ) :
    w = open('contour_band'+str(band)+'.dat' , 'w' ) 
    w.write('## k point cryst coord     energy band '+str(band)+'\n')
    for i in range(len(energy)):
        w.write('  %10.8f   %10.8f        %10.8f\n ' %(k_point[0][i] , k_point[1][i] , energy[i]))





nk=obtain_nK('pw.bands_contour')
nb=obtain_nb('pw.bands_contour')

bands=[39,40,41,42]
k_out=obtains_K_output('pw.bands_contour', int(nk)) 
energy=obtain_energy_output( 'pw.bands_contour', int(nk), int(nb), bands)

for i in range(len(bands)):
    write_output(k_out , energy[:,i] , bands[i])
