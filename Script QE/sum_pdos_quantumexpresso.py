#!/usr/bin/python

import numpy as np



print 'This programm sum pdos of different atoms'
print ''

pdos_file_input = raw_input('## Common parts of the file and orbital  :  ')
pdos_file       = pdos_file_input.split()

atoms_index_in  = raw_input('## Index of atoms  :  ')
atoms_index     = atoms_index_in.split()

atoms_type_in   = raw_input('## Type of atoms  :  ')
atoms_type      = atoms_type_in.split()

name_output_file = raw_input('## Name input file : ')
name_output_file = name_output_file+'_atoms_SUM'
for i in range(len(atoms_index)):
    name_output_file = name_output_file+'_'+atoms_index[i]

file_reference = open(pdos_file[0]+'.pdos_atm#'+atoms_index[0]+'('+atoms_type[0]+')_wfc#'+pdos_file[1],'r').readlines()




def sum_LDOS():
    global pdos_file
    global atoms_index
    global atoms_type
    global file_reference    

    N     = len(file_reference)
    nrows = len(file_reference[1].split())
    LDOS  = np.zeros((N, nrows))   

    for i in range(len(atoms_index)):
        data_file =open( pdos_file[0]+'.pdos_atm#'+atoms_index[i]+'('+atoms_type[i]+')_wfc#'+pdos_file[1],'r').readlines()
        for j in range(1,N):       
            for k in range(1,nrows):
                LDOS[j,k] = LDOS[j,k]+float(data_file[j].split()[k])
    print (LDOS[0,0], LDOS[1,1])
    return LDOS 



def write_LDOS_file(name, LDOS_summed):
    global pdos_file
    global atoms_index
    global atoms_type
    global file_reference

    N     = len(file_reference)
    nrows = len(file_reference[1].split())

    w = open(name, 'w')
    print>>w, file_reference[0]
    for i in range(1,N):
        print>>w, '%8.4f' %(float(file_reference[i].split()[0])),
        for j in range(1,nrows):
             print>>w, '%8.4f' %(LDOS_summed[i,j]),
        print>>w, ''
    w.close()
    print '# output file successfully recorded               : %s ' %name 

LDOS=sum_LDOS()

write_LDOS_file(name_output_file,LDOS)


