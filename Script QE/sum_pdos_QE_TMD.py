#!/usr/bin/python3

import numpy as np



print ('This programm sum pdos of the sametype of atoms using paw full relativistic pseudopotential ')

structure = input('## Enter structure.xsf file   :   ')

pdos_fin = input('## Common parts of the file   :  ')

#atoms_index_in  = input('## Index of atoms  :  ')
#atoms_index     = atoms_index_in.split()

#atoms_type_in   = input('## Type of atoms  :  ')
#atoms_type      = atoms_type_in.split()

name_fout = input('## Name input file : ')




def read_natoms_xsf(file_xsf):

    fin = open( file_xsf, 'r')  ; lines = fin.readlines()   ; fin.close()
    for il in range(len(lines)):
        if (lines[il].split()[0] == 'PRIMCOORD') :
            natoms = int(lines[il+1].split()[0])
    return natoms

def read_structure(file_xsf, natoms):
 
    fin = open( file_xsf, 'r')   ;lines = fin.readlines()   ; fin.close()
    for il in range(len(lines)):
        if ( lines[il].split()[0] == str(natoms) ) :
            line_atoms = lines[il+1:il+natoms+1]

    type_atom  = [line.split()[0] for line in line_atoms ]
    return type_atom

def remove_first_line(lines):
    new_lines = lines[1:len(lines)+1]
    return new_lines

def obtain_E(pdos_in,type_atom_1):
    ldos_in = open( pdos_fin + '.pdos_atm#1(' + type_atom_1 + ')_wfc#1(s_j0.5)','r').readlines()
    E = [ float(line.split()[0]) for line in ldos_in if line.split()[0] != '#']
    return E

def sum_LDOS_type(type_atom, orbital, index_atoms ):
    global pdos_fin
    
    LDOS = [ [] , [] ]
    if (orbital == '_wfc#4(p_j1.5)' or orbital == '_wfc#6(p_j1.5)' )  : LDOS = [ [] , [] , [] , [] ]
    if (orbital == '_wfc#7(d_j1.5)' or orbital == '_wfc#9(d_j1.5)' )  : LDOS = [ [] , [] , [] , [] ]
    if (orbital == '_wfc#8(d_j2.5)' or orbital == '_wfc#10(d_j2.5)' )  : LDOS = [ [] , [] , [] , [] , [] , [] ]



    ldos_in = open( pdos_fin + '.pdos_atm#'+str(index_atoms[0]+1)+'(' + type_atom[0] + ')_wfc#1(s_j0.5)','r')  ; lines = ldos_in.readlines()   ; ldos_in.close() 
    nl = len(lines)-1
    ldos_in.close()

    for i in range( len(index_atoms) ) :
        ldos_lines    = open( pdos_fin + '.pdos_atm#' + str(index_atoms[i]+1) + '(' + type_atom + ')' + orbital,'r').readlines()
        for j in range( len(LDOS) ) :
            LDOS[j] = [ float(line.split()[2+j]) for line in ldos_lines if line.split()[0] != '#']



    return (LDOS) 

def write_LDOS_file(type_atom , energy , LDOS, index_at, orbital):

    N     = len(energy)
    name = type_atom+orbital+'LDOS_sum.dat'
    w = open( name ,'w')
    w.write('#E(eV)    \n')
    w.write('##atoms : ')
    for i in range(len(index_at)):
        w.write('%4s' %str(index_at[i]+1))
    w.write('\n')
    w.write('## '+str(orbital)+'\n')
    if (orbital == '_wfc#4(p_j1.5)' or orbital == '_wfc#6(p_j1.5)' )  :
        for i in range(N):
            w.write(' %10.8f   %12.10f   %12.10f   %12.10f   %12.10f   \n' %(energy[i] , LDOS[0][i] , LDOS[1][i], LDOS[2][i], LDOS[3][i]  ))
    if (orbital == '_wfc#7(d_j1.5)' or orbital == '_wfc#9(d_j1.5)' )  :
        for i in range(N):
            w.write(' %10.8f   %12.10f   %12.10f   %12.10f   %12.10f   \n' %(energy[i] , LDOS[0][i] , LDOS[1][i], LDOS[2][i], LDOS[3][i]  ))
    if (orbital == '_wfc#8(d_j2.5)' or orbital == '_wfc#10(d_j2.5)' ) :
        for i in range(N):
            w.write(' %10.8f   %12.10f   %12.10f   %12.10f   %12.10f   %12.10f   %12.10f   \n' %(energy[i] , LDOS[0][i] ,LDOS[1][i], LDOS[2][i], LDOS[3][i], LDOS[4][i], LDOS[5][i]  ))
    if (orbital == '_wfc#3(p_j0.5)' or orbital == '_wfc#5(p_j0.5)' )  :
        for i in range(N):
            w.write(' %10.8f   %12.10f   %12.10f    \n' %(energy[i] , LDOS[0][i] ,LDOS[1][i]  ))
    if (orbital == '_wfc#1(s_j0.5)' or orbital == '_wfc#2(s_j0.5)' )  :
        for i in range(N):
            w.write(' %10.8f   %12.10f   %12.10f    \n' %(energy[i] , LDOS[0][i] ,LDOS[1][i]  ))
    w.close()
    print ('# output file successfully recorded               : %s ' %name)



def obtain_atom_X_index(X, type_atom):
    X_atom = []
    for i in range(len(type_atom)):
        if type_atom[i] == X : X_atom += [i]
    return X_atom

def obtain_orbital( type_atom ):

    if ( type_atom == 'Mo' or type_atom == 'W' ):
       orbital = [  '_wfc#1(s_j0.5)' , '_wfc#2(s_j0.5)'  ,  '_wfc#3(p_j0.5)' , '_wfc#5(p_j0.5)'  ,  '_wfc#4(p_j1.5)' , '_wfc#6(p_j1.5)'  ,  '_wfc#7(d_j1.5)' , '_wfc#9(d_j1.5)' ,  '_wfc#8(d_j2.5)' , '_wfc#10(d_j2.5)'  ]
       N_orbital = 10
    if ( type_atom == 'S' ) :
        orbital = [  '_wfc#1(s_j0.5)' , '_wfc#2(s_j0.5)'  ,  '_wfc#3(p_j0.5)' , '_wfc#5(p_j0.5)'  ,  '_wfc#4(p_j1.5)' , '_wfc#6(p_j1.5)'  ]
        N_orbital = 6
    
    return (orbital , N_orbital )


### READ STRUCTURE .XSF ###
natoms = read_natoms_xsf(structure)
typ_at = read_structure(structure,natoms)


### OBTAIN ENERGY ###
E = obtain_E(pdos_fin,typ_at[0])


### OBTAIN ATOM TYPE INDEX ###
diff_typ_at = input( '  #### INTRODUCE ALL DIFFERENT TYPE OF ATOMS   : '  )
diff_typ_at = diff_typ_at.split()
N_typ_at = len( diff_typ_at ) 



for k in range ( N_typ_at ) :
    orbital, N_orbital = obtain_orbital( diff_typ_at[k] )
    index = obtain_atom_X_index( diff_typ_at[k] , typ_at )
    for j in range ( N_orbital ) :
        LDOS  = sum_LDOS_type( diff_typ_at[k] , orbital[j] , index  )
        write_LDOS_file( diff_typ_at[k] , E , LDOS , index, orbital[j] )




