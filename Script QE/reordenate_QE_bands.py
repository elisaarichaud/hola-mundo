#!/usr/bin/python3

import  numpy as np




GW_bands = input('### o-qp. GW file :  ')
k_points = int(input('### k point :   '))
n_bands = int(input('### N bands :   '))




def ordenate(eigenvalues_DFT,eigenvalues_QP):
    neig = len(eigenvalues_QP)
    for i in range(1,neig):
        for j in range(i,0,-1):
            if eigenvalues_QP[j]+eigenvalues_DFT[j] < eigenvalues_QP[j-1]+eigenvalues_DFT[j-1] :
                #print(eigenvalues_QP[j]+eigenvalues_DFT[j],eigenvalues_QP[j-1])
                aux_eigen_QP = eigenvalues_QP[j-1]
                aux_eigen_DFT = eigenvalues_DFT[j-1]

                eigenvalues_QP[j-1] =eigenvalues_QP[j]
                eigenvalues_DFT[j-1] =eigenvalues_DFT[j]
                eigenvalues_QP[j] = aux_eigen_QP
                eigenvalues_DFT[j] = aux_eigen_DFT
    return eigenvalues_DFT, eigenvalues_QP



def Remove_comments(lines):
    new_lines = [line for line in lines  if line.split()[0] != '#']
    return new_lines




def read_eigenvalues(GW_file):


    fin = open(GW_file,'r') ; lines = fin.readlines() ; fin.close()
    lines = Remove_comments(lines)
    eigenvalues_DFT = [float(line.split()[2]) for line in lines]
    eigenvalues_QP  = [float(line.split()[3]) for line in lines]
    return eigenvalues_DFT, eigenvalues_QP




def write_eigenvalues(eigenvalues):
    global k_points
    global n_bands

    fout=open('bands_reordenate.dat','w')
    for i in range(k_points):
        eigenvalues_QP = eigenvalues[1][i*n_bands:(i+1)*n_bands]
        eigenvalues_DFT = eigenvalues[0][i*n_bands:(i+1)*n_bands]
        new_eigenvalues = ordenate(eigenvalues_DFT,eigenvalues_QP)
        eigenvalues[1][i*n_bands:(i+1)*n_bands] = new_eigenvalues[1]
        eigenvalues[0][i*n_bands:(i+1)*n_bands] = new_eigenvalues[0]
    for j in range(n_bands):
        for i in range(k_points):
            fout.write('   %2i  %10.8f    %10.8f \n'%(i,eigenvalues[0][j+i*n_bands],eigenvalues[1][j+i*n_bands]) )
        fout.write('\n')
    

eigenvalues = read_eigenvalues(GW_bands)
write_eigenvalues(eigenvalues)


