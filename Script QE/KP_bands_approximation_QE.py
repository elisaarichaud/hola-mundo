#!/usr/bin/python3


import numpy as np
from scipy.optimize import curve_fit
from math import pi
from math import ceil

"""
This program calculate the effective mass of K
approximation for one band from a pw.bands.out 
QE file
"""


def KP( k , m_eff , Eo , vc ) :
    h_bar = 6.582119569E-16 ##eV*Hz-1
   #h_bar = 1.054571817E-34 ##J*Hz-1
    me    = 9.109387015E-31  ##Kg
    eV = 1.60218E-19
     
    if vc == 'valence' : s = -1
    if vc == 'conduction' : s = 1
 
   
    E = Eo + (s * h_bar**2 / me) *( k**2 / (2*m_eff) )

    return (E)


def Remove_Empty_Lines(lines) :
    new_lines = [ line for line in lines if len(line.split()) > 0 ]
    return new_lines


def read_nbnd(in_file) :
    fin = open(in_file, 'r' ) ; lines = fin.readlines() ; fin.close()

    for il in lines : 
        if il.split('=')[0] == '     number of Kohn-Sham states' : nbnd = int( il.split('=')[1] )

    return nbnd


def read_nelectrons(in_file) :
    fin = open(in_file, 'r' ) ; lines = fin.readlines() ; fin.close()

    for il in lines : 
        if il.split('=')[0] == '     number of electrons       ' : nelectrons = float( il.split('=')[1] )

    return nelectrons


def read_kpoints(in_file):
    fin = open(in_file,'r') ; lines = fin.readlines() ; fin.close()
    A = 0.529177   # 1 Bo = A Angstrom

   # b = [ [] , [] , [] ]
    k = []
    for il in range( len(lines) ) :
        if lines[il].split('=')[0] == '     lattice parameter (alat)  ' : 
            alat = float( lines[il].split()[4] ) ; blat = (2*pi)/alat
            blat = blat/A

       #if lines[il].split(':')[0] == '     reciprocal axes' :
       #   b[0] = list( map(float, lines[il+1].split()[3:6] ) )
       #   b[1] = list( map(float, lines[il+2].split()[3:6] ) )
       #   b[2] = list( map(float, lines[il+3].split()[3:6] ) )
       #   print(b)
        if lines[il].split('=')[0] == '     number of k points' : nkpt = int( lines[il].split('=')[1] ) ; il_nkpt = il 

    k_lines = lines[ il_nkpt+2 : il_nkpt+2+nkpt ]
    
    k = [ blat * (float( line.split()[4] ) **2 + float( line.split()[5] )**2 + float( line.split()[6].replace('),','') ) **2)  for line in k_lines ]
    print(blat)

    return ( nkpt , k )
        


def read_energies(in_file, bnd ,kpt) :
    fin = open(in_file, 'r') ; lines = fin.readlines() ; fin.close()
    lines = Remove_Empty_Lines(lines)

    E = []
    k = -1
    for il in range( len(lines) ) :
        if lines[il].split('=')[0] == '          k ' :
            k += 1
            if ( k <= 1 ) :
               row = ceil(bnd/8)
               col = bnd - row*8 + 7
               E.append( float( lines[il+row].split()[col] ) )
            if ( k > 1 ) :
                dk_old = kpt[k-1] -kpt[k-2]
                dE_old = ( E[k-1] - E[k-2] )/ dk_old
                E_old  = E[k-1]

                row = [ ceil( (bnd-1)/8 ) , ceil( bnd/8 ) , ceil( (bnd+1)/8 ) ]
                col = [ (bnd-1) - row[0]*8 + 7 , bnd-row[1]*8 + 7 , (bnd+1) - row[2]*8 + 7 ]
                E_new = [ float( lines[il+row[n]].split()[col[n]] ) for n in range(3) ]
                dk_new =  kpt[k] - kpt[k-1]
                dE_new = [ ( ( E_new[n] - E_old )/dk_new )  for n in range(3) ]
                
                d2E = [ abs(dE_new[n] - dE_old) for n in range(3) ]
                if ( d2E.index( min(d2E) ) == 0 ) : bnd = bnd-1 ; E.append( E_new[0] )
                if ( d2E.index( min(d2E) ) == 1 ) : bnd = bnd   ; E.append( E_new[1] )
                if ( d2E.index( min(d2E) ) == 2 ) : bnd = bnd+1 ; E.append( E_new[2] )
                print(d2E.index( min(d2E)))

    return E




###########

## Input variables
#fin = input( '#### Bands file    : ' )
#bnd = int( input( '#### Bands index   : ' ) )
kpt_select =  input('#### Kpoint index // start and end   :') 
kpt_select = list( map( int, kpt_select.split() ) )


fin = 'pw.bands.out'
bnd = 41


## Read data from pw.bands.out
nv = int(read_nelectrons(fin) // 2)

nkpt, kpt = read_kpoints(fin)
energy    = read_energies(fin, bnd, kpt )
energy = energy[kpt_select[0]-1 : kpt_select[1]]
kpt = kpt[kpt_select[0]-1 : kpt_select[1]]


if bnd <= nv : state = 'valence' ; Eo = max(energy)
if bnd > nv  : state = 'conduction' ; Eo = min(energy)


#curve_fit(KP(Eo = Eo, vc = state),kpt,energy)
m_eff, err = curve_fit( lambda x , m_eff : KP(x, m_eff, Eo=Eo, vc=state), kpt, energy)
print(m_eff, err)

fout = open('toto.dat','w')
for i in range(len(kpt)):
    #print(energy[i] , 'KP=', KP(kpt[i],m_eff,Eo,state) )
    fout.write( '%8.6f   %8.6f\n' %(kpt[i], KP(kpt[i],m_eff,Eo,state)))

fout.close()
        
        







