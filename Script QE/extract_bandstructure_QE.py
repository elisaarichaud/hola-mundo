#!/usr/bin/python3

import numpy as np


file_in = input('#####  Input file  :')



def Remove_Empty_Lines(lines) :
    new_lines = [ line for line in lines if len(line.split()) > 0 ]
    return new_lines


def Read_kpoints(filein):
    index = []
    ni = 0
    fin = open(filein, 'r') ; lines = fin.readlines() ; fin.close()
    lines = Remove_Empty_Lines(lines)
    for i in range(len(lines)):
        if lines[i].split()[0] == 'k' :
            index.append(i)
            ni+=1

            
    kpoint = np.zeros([ni,3])
    j=0
    for i in range(len(lines)):
        if lines[i].split()[0] == 'k' :
            kpoint[j,0] = float(lines[i][13:19])
            kpoint[j,1] = float(lines[i][20:26])
            kpoint[j,2] = float(lines[i][27:33])
            j+=1
    return (kpoint, index)

def Read_nbands(filein) :
    fin = open(filein, 'r' ) ; lines = fin.readlines() ; fin.close()

    for il in lines :
        if il.split('=')[0] == '     number of Kohn-Sham states' : nbnd = int( il.split('=')[1] )

    return nbnd



def Read_energy(filein):
    global nbands
    global bands_index
    global index_kpoint
    fin = open(filein, 'r') ; lines = fin.readlines() ; fin.close()
    lines = Remove_Empty_Lines(lines)
    energy = np.zeros([len(index_kpoint),nbands])
    for i in range(len(index_kpoint)):
        for j in range(nbands):
            column = int(int(bands_index[j])%8)
            if column == 0 : 
                row =  int(int(bands_index[j])/8)
            else :
                row = 1 + int(int(bands_index[j])/8)
            energy[i,j] = float(lines[int(index_kpoint[i])+row].split()[column-1])
    return(energy)




def write_output(filein):
    global nbands
    global bands_index
    global kpoint
    global index_kpoint
    global energy

    fout = open('bandstructure.dat', 'w')
    fout.write('##%20s \n' %'Bandstructure')
    for i in range(nbands):
        fout.write('##%6s  %2i\n' %('band',i+1))
        for j in range(len(index_kpoint)):
            fout.write('%3i    %8.6f\n'   %(j,energy[j,i]))
        fout.write('\n')    
    fout.close()



kpoint       = Read_kpoints(file_in)[0]
index_kpoint = Read_kpoints(file_in)[1]
nbands       = Read_nbands(file_in)
bands_index  = np.arange(1,nbands+1)
energy       = Read_energy(file_in) 

write_output(file_in)
print(nbands)


