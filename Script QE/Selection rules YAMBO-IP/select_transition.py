#!/usr/bin/python3



from dipolesdb import *
from yambopy.dbs.electronsdb import *
import numpy as np
from yambopy.dbs.latticedb import *
from qepy.projwfcxml import *
from yambopy.tools.funcs import abs2,lorentzian, gaussian


def write_ip(ip_spectra1,ip_spectra2,fout):
    l = len(ip_spectra1[0])
    wout = open(fout,'w')
    wout.write('## IP absortion spectra \n')
    
    for i in range(l):
        wout.write('  %10.8f     %10.8f      %10.8f \n' %(ip_spectra1[0][i] , ip_spectra1[1][i] , ip_spectra2[1][i]))

    wout.close()




def create_selected_matrix(kp,nbnd, kp_select, ic_select, iv_select):

    ## kp (integer)   : number of k points
    ## nbnd (integer) : total number of bands
    ## kp_select (list) : index of kpoints to frobidden
    ## ic_select (list) : index list of conduction band to forbidden
    ## iv_select (list) : index list of valence band to forbidden
    ## Only transitions from iv_selct[i] to ic_select[i] at kp_selct[i] is forbidden


    w_select = np.ones([kp,nbnd,nbnd])
    l =len(kp_select)

    if (l!= len(ic_select)) : "Bad dimension of select list"
    if (l!= len(iv_select)) : "Bad dimension of select list"

    for i in range(l):
        w_select[ kp_select[i] , int(ic_select[i]) , int(iv_select[i]) ] = 0

    return w_select

   

def forbide_kpoint(kp,nbnd ,kp_select):

    w_select = np.ones([kp,nbnd,nbnd])

    for iv in range(nbnd):
        for ic in range(nbnd):
            w_select[kp_select,ic,iv] = 0
    return w_select


def select_transition(kp,nbnd, ic_select,iv_select):

    #w_select = np.ones([kp,nbnd,nbnd])
    w_select = np.zeros([kp,nbnd,nbnd])
    for l in range(len(ic_select)):
        for k in range(kp):
            #w_select[k,ic_select[l],iv_select[l]] = 0
            w_select[k,ic_select[l],iv_select[l]] = 1
    return w_select




ylat       = YamboLatticeDB.from_db_file(filename='SAVE/ns.db1')
yelectrons = YamboElectronsDB(ylat,save='SAVE/')
ydipoles   = YamboDipolesDB(ylat,save='RPA_db/',filename='ndb.dipoles')

###############
###############  ip_eps2(self,electrons,pol=1,ntot_dip=-1,GWshift=0.,broad=0.1,broadtype='l',nbnds=[-1,-1],emin=0.,emax=10.,esteps=500,w_select=None)
##############




## kp=120  nbnd=168  tv=56  
kp =30
nbnd=150
ic_select=34
iv_select=33

ic_select=[38]
iv_select=[29]


w_select = select_transition(kp,nbnd, ic_select, iv_select)
ip_pol1 = ydipoles.ip_eps2(yelectrons,pol=1,broad=0.12, nbnds=[56,112],esteps=1000, w_select=w_select)
ip_pol2 = ydipoles.ip_eps2(yelectrons,pol=0,broad=0.12, nbnds=[56,112],esteps=1000, w_select=w_select)


write_ip(ip_pol1, ip_pol2, 'eps_wo_'+str(iv_select[0])+'-'+str(ic_select[0])+'.dat')







