import unittest
import os
#from yambopy.dbs.dipolesdb import *
# from yambopy.dbs.bubu import *
from dipolesdb import *
from yambopy.dbs.electronsdb import *
import numpy as np
from yambopy.dbs.latticedb import *
from qepy.projwfcxml import *
from yambopy.tools.funcs import abs2,lorentzian, gaussian
import copy


test_path = os.path.join(os.path.dirname(__file__),'..','data','refs','hBN-2D','test')



def ip_eps(dipoles,electrons, pol=1, ntot_dip=-1,GWshift=0.,broad=0.1,broadtype='l',nk=4,nbnds=[-1,-1],emin=0.,emax=10.,esteps=100,w_select=None): 
        #get eigenvalues and weights of electrons
        eiv = electrons.eigenvalues
        #print(eiv.shape)
        weights = electrons.weights
        nv = electrons.nbandsv
        nc = electrons.nbandsc
        nk = electrons.nkpoints


        #get frequencies and im
        freq = np.linspace(emin,emax,esteps)
        eps2 = np.zeros([len(freq)])

        #Cut bands to the maximum number used for the dipoles
        if ntot_dip>0:
            eiv = eiv[:,:ntot_dip]
            nc=ntot_dip-nv

        #Print band gap values and apply GW_shift
        electrons.energy_gaps(GWshift)

        #Check bands to include in the calculation
        if nbnds[0]<0: nbnds[0]=nv
        if nbnds[1]<0: nbnds[1]=nc
        iv = nv-nbnds[0] #first valence
        lc = nv+nbnds[1] #last conduction

        #choose broadening
        if "l" in broadtype:
            broadening = lorentzian
        else:
            broadening = gaussian

        #Selected transition weight
        if w_select is None:
           w_select = np.ones([nk,nc+nv,nc+nv])

        na = np.newaxis
        #calculate epsilon
        for c,v in product(range(nv,lc),range(iv,nv)):

                #get electron-hole energy and dipoles
                ecv  = eiv[:,c]-eiv[:,v]
                dip2 = np.abs(dipoles[:,pol,c,v])**2.
                w_s2 = w_select[:,c,v]

                #make dimensions match
                dip2a = dip2[na,:]
                ecva  = ecv[na,:]
                freqa = freq[:,na]
                wa    = weights[na,:]
                w_s2a = w_s2[na,:]

                #calculate the lorentzians
                broadw = broadening(freqa,ecva,broad)

                #scale broadening with dipoles and weights
                epsk =  wa*dip2a*broadw*w_s2a

                #integrate over kpoints
                eps2 += np.sum(epsk,axis=1)

        return freq, eps2

 




class Test_ip_weight(unittest.TestCase):

        
    def test_default_weight(self):
        lat = YamboLatticeDB.from_db_file(filename=test_path+'/YAMBOPY_test/SAVE/ns.db1')
        electrons = YamboElectronsDB(lat,save=test_path+'/YAMBOPY_test/SAVE/')
        dipoles = YamboDipolesDB(lat,save=test_path+'/YAMBOPY_test/yambopy_test_db/',filename='ndb.dipoles')
        dip = dipoles.ip_eps2(electrons,esteps=100)

        dip_ref_file = open(test_path+'/YAMBOPY_test/IP_spectra.dat','r') ; dip_ref = dip_ref_file.readlines() ; dip_ref_file.close()
        print(dip[1]) 
        for i in range(len (dip[0])):

            self.assertEqual(dip[1][i],float(dip_ref[i+2].split()[1]))



    def test_weight_0(self):
        lat = YamboLatticeDB.from_db_file(filename=test_path+'/YAMBOPY_test/SAVE/ns.db1')
        electrons = YamboElectronsDB(lat,save=test_path+'/YAMBOPY_test/SAVE/')
        dipoles = YamboDipolesDB(lat,save=test_path+'/YAMBOPY_test/yambopy_test_db/',filename='ndb.dipoles')
        w_select = np.zeros([4,8,8])
        dip = dipoles.ip_eps2(electrons,w_select=w_select)

        
        for i in range(len (dip[0])):

            self.assertEqual(dip[1][i],0)



    def test_weight_1(self):
        lat = YamboLatticeDB.from_db_file(filename=test_path+'/YAMBOPY_test/SAVE/ns.db1')
        electrons = YamboElectronsDB(lat,save=test_path+'/YAMBOPY_test/SAVE/')
        dipoles_set = YamboDipolesDB(lat,save=test_path+'/YAMBOPY_test/yambopy_test_db/',filename='ndb.dipoles')
        dipoles_ref = copy.deepcopy(dipoles_set.dipoles)
        w_set = np.ones([4,8,8])

        dipoles_ref[1,:,:]=0
        w_set[1,:,:]=0


        dip_set = dipoles_set.ip_eps2(electrons,esteps=100,w_select=w_set)
        dip_ref = ip_eps(dipoles_ref,electrons)
        
        for i in range(len (dip_set[0])):

            self.assertEqual(dip_set[1][i],dip_ref[1][i])



    def test_weight_2(self):
        lat = YamboLatticeDB.from_db_file(filename=test_path+'/YAMBOPY_test/SAVE/ns.db1')
        electrons = YamboElectronsDB(lat,save=test_path+'/YAMBOPY_test/SAVE/')
        dipoles_set = YamboDipolesDB(lat,save=test_path+'/YAMBOPY_test/yambopy_test_db/',filename='ndb.dipoles')
        dipoles_ref = copy.deepcopy(dipoles_set.dipoles)
        w_set = np.ones([4,8,8])

        dipoles_ref[:,1,5,4]=0
        w_set[:,5,4]=0


        dip_set = dipoles_set.ip_eps2(electrons,esteps=100,w_select=w_set)
        dip_ref = ip_eps(dipoles_ref,electrons)
       
        print(dip_set[1],dip_ref[1]) 

        for i in range(len (dip_set[0])):

            self.assertEqual(dip_set[1][i],dip_ref[1][i])







if __name__ == '__main__':
    unittest.main()



